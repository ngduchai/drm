#include <iostream>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/stdcxx.h>

#include "../../gen-cpp/ResourcePartitioner.h"


using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using namespace drm;

int main() {
  stdcxx::shared_ptr<TTransport> socket(new TSocket("localhost", 12345));
  stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
  stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
  ResourcePartitionerClient client(protocol);

  try {
    transport->open();

    std::set<PoolWeight> weights;
    client.getWeight(weights);
    for (auto weight : weights) {
      cout << weight.id << ": " << weight.weight;
    }
  } catch (apache::thrift::transport::TTransportException& ex) {
    cout << "Request failed " << ex.what();
  }

  return 0;
}


