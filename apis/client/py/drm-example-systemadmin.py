#!/usr/bin/env python

import sys
sys.path.append('gen-py')

from drm import ResourcePartitionerInterface;
from drm import SystemAdminInterface;
from drm import ResourcePoolOwnerInterface;
from drm import PerformanceStatisticsInterface;
from drm.ttypes import *
from drm.constants import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol.TMultiplexedProtocol import TMultiplexedProtocol

# Make socket
transport = TSocket.TSocket('localhost', 12345)

# Buffering is critical. Raw sockets are very slow
transport = TTransport.TBufferedTransport(transport)

# Wrap in a protocol
protocol = TBinaryProtocol.TBinaryProtocol(transport)

# Create a client to use the protocol encoder
rp_mp = TMultiplexedProtocol(protocol, "Resource Partitioner")
rp_client = ResourcePartitionerInterface.Client(rp_mp)

rpo_mp = TMultiplexedProtocol(protocol, "Resource Pool Owner")
rpo_client = ResourcePoolOwnerInterface.Client(rpo_mp)

sa_mp = TMultiplexedProtocol(protocol, "System Admin")
sa_client = SystemAdminInterface.Client(sa_mp)

ps_mp = TMultiplexedProtocol(protocol, "Performance Statistices")
ps_client = PerformanceStatisticsInterface.Client(ps_mp)

# Connect!
transport.open()

example = Role("example", 2)

print "add example"
print "-->", sa_client.addRole(example)

print "delete old example"
print " -->", sa_client.deleteRole("example")

print "add example again"
print " -->", sa_client.addRole(example)




