#!/usr/bin/env python

import sys
sys.path.append('../gen-py')
#import drm

from urllib2 import HTTPError

from drm import ResourcePartitionerInterface;
from drm import SystemAdminInterface;
from drm import ResourcePoolOwnerInterface;
from drm import PerformanceStatisticsInterface;
from resthttp import *;
from drm.ttypes import *
from drm.constants import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
from thrift.TMultiplexedProcessor import TMultiplexedProcessor

roles = {};
mesos_url = "http://localhost:5050"

class ResourcePartitionerHandler:
	def __init__(self):
		self.log = {}

	def setWeight(self, role, weight):
		code = 0;
		msg = 'success';
		try:
			# Type checking
			if not isinstance(role, basestring):
				raise ValueError("name is not a string");
			if not isinstance(weight, int) and not isinstance(weight, float) and not isinstance(weight, float):
				raise ValueError("weight is not a float");
			# Sign checking
			if weight < 0:
				raise ValueError("weight is negative");
			# Existence checking
			elif role not in roles:
				code = 3;
				msg = "Role does not exist";
			else:
				roles[role].weight = weight;
		except KeyError as error:
			code = 3;
			msg = str(error) + ' is missing';
		except ValueError as error:
			code = 3;
			msg = str(error);
		return Response(ErrorInfo(code, msg))

	def getWeight(self, role):
		if role not in roles:
			raise DRMException(ErrorInfo(1, "Unknown role: " + role))
		else:
			return roles[role].weight;

class SystemAdminHandler:
	def __init__(self):
		self.log = {}
	
	def addRole(self, newrole):
		code = 0;
		msg = 'success';
		try:
			if not isinstance(newrole, Role):
				raise ValueError("newrole has invalid datatype: " + type(newrole));
			weight = newrole.weight;
			if weight < 0:
				raise ValueError("weight is negative");
			
			if newrole.name in roles:
				code = 3;
				msg = "Role exists";
			else:
				# Update role's weight
				payload = {}
				url = mesos_url + "/weights";
				payload['role'] = newrole.name;
				payload['weight'] = newrole.weight
				data = [];
				data.append(payload);
				res = rest_call_json(url, data, 'PUT');
				print res
				roles[newrole.name] = newrole
		except KeyError as error:
			code = 3;
			msg = str(error) + ' is missing';
		except ValueError as error:
			code = 3;
			msg = str(error);
		except HTTPError as error:
			code = 1;
			msg = str(error);
		except Exception as error:
			code = 1;
			msg = str(error);
		return Response(ErrorInfo(code, msg));
	
	
	def deleteRole(self, role):
		code = 0;
		msg = 'success';
    		try:
        		del roles[role];
			# Disable role
			payload = {}
			url = mesos_url + "/weights";
			payload['role'] = role;
			payload['weight'] = 0.0000000001
			data = [];
			data.append(payload);
			res = rest_call_json(url, data, 'PUT');
			print res
		except KeyError:
			code = 3;
			msg = "Given role does not exist";
		except HTTPError as error:
			code = 1;
			msg = str(error);
		return Response(ErrorInfo(code, msg));
	
	def addResource(self, resources):
		raise DRMException(ErrorInfo(1, "Unimplemented method!"));
	
	def start(self, hostname, port):
		raise DRMException(ErrorInfo(1, "Unimplemented method!"));
	
	def shutdown(self, hostname, port):
		raise DRMException(ErrorInfo(1, "Unimplemented method!"));

class ResourcePoolOwnerHandler:
	def __init__(self):
		self.log = {}
	
	def subscribe(self, poolId, roles):
		code = 0;
		msg = 'success';
		return Response(ErrorInfo(code, msg));
	
        def desubscribe(self, poolId, roles):
		code = 0;
		msg = 'success';
		return Response(ErrorInfo(code, msg));

        def acceptOffer(self, offer,  opt, filter):
		code = 0;
		msg = 'success';
		return Response(ErrorInfo(code, msg));
        
	def declineOffer(self, offer, filter):
		code = 0;
		msg = 'success';
		return Response(ErrorInfo(code, msg));

	def releaseResource(resources):
		code = 0;
		msg = 'success';
		return Response(ErrorInfo(code, msg));

class PerformanceStatisticsHandler:
	def __init__(self):
		self.log = {}
	
	def retrieveRoleInformation(role):
		code = 0;
		msg = 'success';
		raise DRMException(ErrorInfo(code, msg));
	
        def retrieveRoleAllocation(role):
        	code = 0;
		msg = 'success';
		raise DRMException(ErrorInfo(code, msg));
	
	def retrievePoolAllocation(poolId):
		code = 0;
		msg = 'success';
		raise DRMException(ErrorInfo(code, msg));
	

rp_handler = ResourcePartitionerHandler()
rp_processor = ResourcePartitionerInterface.Processor(rp_handler)

sa_handler = SystemAdminHandler()
sa_processor = SystemAdminInterface.Processor(sa_handler)

rpo_handler = ResourcePoolOwnerHandler()
rpo_processor = ResourcePoolOwnerInterface.Processor(rpo_handler)

ps_handler = PerformanceStatisticsHandler()
ps_processor = PerformanceStatisticsInterface.Processor(ps_handler)

transport = TSocket.TServerSocket(12345)
tfactory = TTransport.TBufferedTransportFactory()
pfactory = TBinaryProtocol.TBinaryProtocolFactory()

processor = TMultiplexedProcessor()
processor.registerProcessor("Resource Partitioner", rp_processor);
processor.registerProcessor("Resource Pool Owner", rpo_processor);
processor.registerProcessor("System Admin", sa_processor);
processor.registerProcessor("Performance Statistices", ps_processor);

server = TServer.TThreadedServer(processor, transport, tfactory, pfactory)

print "Starting python server..."
server.serve()
print "done!"



