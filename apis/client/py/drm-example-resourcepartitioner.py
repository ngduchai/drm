#!/usr/bin/env python

import sys
sys.path.append('../gen-py')

from drm import ResourcePartitionerInterface;
from drm import SystemAdminInterface;
from drm import ResourcePoolOwnerInterface;
from drm import PerformanceStatisticsInterface;
from drm.ttypes import *
from drm.constants import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol.TMultiplexedProtocol import TMultiplexedProtocol


# Make socket
transport = TSocket.TSocket('localhost', 12345)

# Buffering is critical. Raw sockets are very slow
transport = TTransport.TBufferedTransport(transport)

# Wrap in a protocol
protocol = TBinaryProtocol.TBinaryProtocol(transport)

# Create a client to use the protocol encoder
rp_mp = TMultiplexedProtocol(protocol, "Resource Partitioner")
rp_client = ResourcePartitionerInterface.Client(rp_mp)

rpo_mp = TMultiplexedProtocol(protocol, "Resource Pool Owner")
rpo_client = ResourcePoolOwnerInterface.Client(rpo_mp)

sa_mp = TMultiplexedProtocol(protocol, "System Admin")
sa_client = SystemAdminInterface.Client(sa_mp)

ps_mp = TMultiplexedProtocol(protocol, "Performance Statistices")
ps_client = PerformanceStatisticsInterface.Client(ps_mp)

# Connect!
transport.open()

print "Get weight"
print " -->", rp_client.getWeight("example")



'''
print "Set weight = -1"
print " -->", rp_client.setWeight("example", -1)

print "Set weight = 10"
print " -->", rp_client.setWeight("example", 10)

print "Get new weight"
print " -->", rp_client.getWeight("example")


def processOffer(offers):
    # Resource in accepted offers will be used for launching tasks
    opt = LAUNCH
    # Filter duration = 5 seconds
    filter = Filter(5)
    
    # Initialize new task(s) information, including environment variables, command, etc.  
    task = ...
    # new tasks need 1 cpu and 1024MB memory
    task.resources.cpus = 1
    task.resources.mem = 1024
    opt.task = task

    # Look at offers
    for offer in offers:
        if offer.resources.cpus >= 1 and offer.resources.mem >= 1024:
            client.acceptOffer(offer, opt, filter)
	else:
            client.declineOffer(offer, filter)

# runningTasks is a list of tasks generating by the pool and currently running
runningTasks = ...

def processReclaimResource(resources, timestamp, time):
    # sum of return resources, initialized by setting 0 for each type of resources
    sumReturned = ...
    
    # we remove terminated tasks from a copy of runningTasks and update it later
    tempRunningTasks = {}
    
    # set of resources to be returned to DRM
    returnedResources = set()
    
    # trigger randomization by shuffling the list of running tasks
    shuffle(runningTasks)
    i = 0
    # Look through running tasks ...
    while i < range(len(runningTasks)):
        # ... and terminate them one by one
        res = client.sendSignal(runningTasks[i], Signal.KILL)
        if res.code == 0:
            # if the task terminated, then return its resources
            returnedResource.add(runnningTasks[i].resources
            sumReturn += runningTasks[i];
            # if sum of returned resources < or = request then stop terminating tasks
            if sumReturn >= resources:
                break
        else:
            # the task is not terminated, keep it in the list of running tasks
            tempRunningTasks.append(runningTasks[i])
        i = i + 1
    
    # Update the list running tasks after terminating some tasks
    while i < range(len(runningTasks)):
        tempRunningTasks.append(runningTasks[i])
    runningTasks = tempRunningTasks
    
    # Returned resources to DRM
    client.releaseResources(returnedResources)
'''




