/*
 * Copyright 2017-present Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include <folly/SocketAddress.h>
#include <folly/init/Init.h>
#include <folly/io/async/EventBase.h>

//#include <thrift/example/cpp2/server/EchoService.h>
//#include <thrift/example/if/gen-cpp2/Echo.h>
//#include <thrift/lib/cpp2/transport/core/testutil/ServerConfigsMock.h>

//#include "util/Util.h"


//#include "../../gen-cpp2/ResourcePartitionerInterfaceAsyncClient.h"


using namespace std;
using namespace apache::thrift;
using namespace drm::cpp2;

const string HOSTNAME = "127.0.0.1";
const int PORT = 12345;
const string TRANSPORT = "header";
// Transport to use: header, rsocket, http2, or inmemory;

//using apache::thrift::server::ServerConfigsMock;

int main(int argc, char* argv[]) {
  FLAGS_logtostderr = true;
  folly::init(&argc, &argv);

  // Create a thrift client
  auto addr = folly::SocketAddress(HOSTNAME, PORT);
  auto ct = std::make_shared<ConnectionThread<ResourcePartitionerInterfaceAsyncClient>>();
  auto client = ct->newSyncClient(addr, TRANSPORT);
  LOG(INFO) << "start client"; 
  // For header transport
  
  folly::EventBase evb;
  if (TRANSPORT == "header") {
    client = newHeaderClient<ResourcePartitionerInterfaceAsyncClient>(&evb, addr);
  }
  
  // For inmemory transport
  /*
  auto handler = std::make_shared<EchoHandler>();
  ServerConfigsMock serverConfigs;
  if (TRANSPORT == "inmemory") {
    client =
        newInMemoryClient<ResourcePartitionerInterfaceAsyncClient, EchoHandler>(handler, serverConfigs);
  }
  */

  //std::string message = "Ping this back";
  //std::string response;

  // Get an echo'd message
  try {
    std::set<PoolWeight> weights;
    client->sync_getWeight(weights);
    for (auto weight : weights) {
      LOG(INFO) << weight.id << ": " << weight.weight;
    }
  } catch (apache::thrift::transport::TTransportException& ex) {
    LOG(ERROR) << "Request failed " << ex.what();
  }

  return 0;
}


