#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/PlatformThreadFactory.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/TToString.h>
#include <thrift/stdcxx.h>

#include <iostream>
#include <stdexcept>
#include <sstream>

#include "../gen-cpp/ResourcePartitioner.h"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

using namespace drm;

class ResourcePartitionerHandler : public ResourcePartitionerIf {
public:
  ResourcePartitionerHandler() {}
  
  virtual void getWeight(std::set<PoolWeight> & _return) {
    cout << "getWeight" << endl;
    cout << _return.size() << endl;
    PoolWeight temp;
    temp.id = "role1";
    temp.weight = 1.0;
    _return.insert(temp);
  }

  virtual void connect(Response& _return, const SecurityCredential& credential) {}
  virtual void disconnect(Response& _return, const SecurityCredential& credential) {}
  virtual void setWeight(Response& _return, const std::set<PoolWeight> & poolWeights) {}


  void zip() { cout << "zip()" << endl; }

};

/*
 *   ResourcePartitionerIfFactory is code generated.
 *   ResourcePartitionerCloneFactory is useful for getting access to the server side of the
 *   transport.  It is also useful for making per-connection state.  Without this
 *   CloneFactory, all connections will end up sharing the same handler instance.
 **/
class ResourcePartitionerCloneFactory : virtual public ResourcePartitionerIfFactory {
 public:
  virtual ~ResourcePartitionerCloneFactory() {}
  virtual ResourcePartitionerIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo)
  {
    stdcxx::shared_ptr<TSocket> sock = stdcxx::dynamic_pointer_cast<TSocket>(connInfo.transport);
    cout << "Incoming connection\n";
    cout << "\tSocketInfo: "  << sock->getSocketInfo() << "\n";
    cout << "\tPeerHost: "    << sock->getPeerHost() << "\n";
    cout << "\tPeerAddress: " << sock->getPeerAddress() << "\n";
    cout << "\tPeerPort: "    << sock->getPeerPort() << "\n";
    return new ResourcePartitionerHandler;
  }
  virtual void releaseHandler(ResourcePartitionerIf* handler) {
    delete handler;
  }
};

int main() {
  TThreadedServer server(
    stdcxx::make_shared<ResourcePartitionerProcessorFactory>(stdcxx::make_shared<ResourcePartitionerCloneFactory>()),
    stdcxx::make_shared<TServerSocket>(12345), //port
    stdcxx::make_shared<TBufferedTransportFactory>(),
    stdcxx::make_shared<TBinaryProtocolFactory>());

  cout << "Starting the server..." << endl;
  server.serve();
  cout << "Done." << endl;
  return 0;

    
}



