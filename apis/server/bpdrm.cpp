
#include <folly/init/Init.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
//#include <proxygen/httpserver/HTTPServerOptions.h>

#include <thrift/lib/cpp2/server/ThriftServer.h>
#include <thrift/lib/cpp2/transport/core/ThriftProcessor.h>
#include <thrift/lib/cpp2/transport/http2/common/HTTP2RoutingHandler.h>
#include <thrift/lib/cpp2/transport/rsocket/server/RSRoutingHandler.h>
#include <thrift/lib/cpp2/server/TransportRoutingHandler.h>


#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>


#include "../gen-cpp2/ResourcePartitionerInterface.h"

using apache::thrift::HTTP2RoutingHandler;
using apache::thrift::ThriftServer;
using apache::thrift::ThriftServerAsyncProcessorFactory;
using namespace apache::thrift::server;

using proxygen::HTTPServerOptions;
using proxygen::RequestHandlerChain;

using namespace std;

using namespace drm::cpp2;

class ResourcePartitionerHandler : virtual public ResourcePartitionerInterfaceSvIf {
public:
	ResourcePartitionerHandler() {};

	virtual void getWeight(std::set< ::drm::cpp2::PoolWeight>& pw) {
		LOG(INFO) << "get weight";
		PoolWeight temp;
		temp.id = "role1";
		temp.weight = 1.0;
		pw.insert(temp);
		temp.id = "role2",
		temp.weight = 2.0;
		pw.insert(temp);
	}

};

std::unique_ptr<HTTP2RoutingHandler> createHTTP2RoutingHandler(
		std::shared_ptr<ThriftServer> server) {
	auto h2_options = std::make_unique<HTTPServerOptions>();
	h2_options->threads = static_cast<size_t>(server->getNumIOWorkerThreads());
	h2_options->idleTimeout = server->getIdleTimeout();
	h2_options->shutdownOn = {SIGINT, SIGTERM};
	return std::make_unique<HTTP2RoutingHandler>(
		std::move(h2_options), server->getThriftProcessor(), *server);
}

template <typename ServiceHandler>
	std::shared_ptr<ThriftServer> newServer(int32_t port) {
	auto handler = std::make_shared<ServiceHandler>();
	auto proc_factory =
		std::make_shared<ThriftServerAsyncProcessorFactory<ServiceHandler>>(handler);
	auto server = std::make_shared<ThriftServer>();
	server->setPort(port);
	server->setProcessorFactory(proc_factory);
	
	server->addRoutingHandler(
			std::make_unique<apache::thrift::RSRoutingHandler>());
	server->addRoutingHandler(createHTTP2RoutingHandler(server));
	
	return server;
}

int main(int argc, char** argv) {
	FLAGS_logtostderr = 1;
	folly::init(&argc, &argv);
	int32_t drm_port = 12345;
	
	auto drm_server = newServer<ResourcePartitionerHandler>(drm_port);
	LOG(INFO) << "DRM Server running on port: " << drm_port;
	
	drm_server->serve();

	return 0; 
}         
          
          
          
