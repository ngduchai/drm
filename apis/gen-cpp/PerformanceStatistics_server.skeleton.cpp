// This autogenerated skeleton file illustrates how to build a server.
// You should copy it to another filename to avoid overwriting it.

#include "PerformanceStatistics.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using namespace  ::drm;

class PerformanceStatisticsHandler : virtual public PerformanceStatisticsIf {
 public:
  PerformanceStatisticsHandler() {
    // Your initialization goes here
  }

  void retrievePoolAllocation(Resource& _return, const String& poolId) {
    // Your implementation goes here
    printf("retrievePoolAllocation\n");
  }

  void retrieveAllPoolsAllocation(std::set<Resource> & _return) {
    // Your implementation goes here
    printf("retrieveAllPoolsAllocation\n");
  }

};

int main(int argc, char **argv) {
  int port = 9090;
  ::apache::thrift::stdcxx::shared_ptr<PerformanceStatisticsHandler> handler(new PerformanceStatisticsHandler());
  ::apache::thrift::stdcxx::shared_ptr<TProcessor> processor(new PerformanceStatisticsProcessor(handler));
  ::apache::thrift::stdcxx::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  ::apache::thrift::stdcxx::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  ::apache::thrift::stdcxx::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
  return 0;
}

