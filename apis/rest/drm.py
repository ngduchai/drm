#!/usr/bin/env python

'''
Simple and functional REST server for Python (2.7) using no dependencies beyond the Python standard library.

Features:

* Map URI patterns using regular expressions
* Map any/all the HTTP VERBS (GET, PUT, DELETE, POST)
* All responses and payloads are converted to/from JSON for you
* Easily serve static files: a URI can be mapped to a file, in which case just GET is supported
* You decide the media type (text/html, application/json, etc.)
* Correct HTTP response codes and basic error messages
* Simple REST client included! use the rest_call_json() method

As an example, let's support a simple key/value store. To test from the command line using curl:

curl "http://localhost:8080/roles"
curl -X PUT -d '{"name": "Tal"}' "http://localhost:8080/role/1"
curl -X PUT -d '{"name": "Shiri"}' "http://localhost:8080/role/2"
curl "http://localhost:8080/roles"
curl -X DELETE "http://localhost:8080/role/2"
curl "http://localhost:8080/roles"

Create the file web/index.html if you'd like to test serving static files. It will be served from the root URI.

@author: Tal Liron (tliron @ github.com)
'''

import sys, os, re, shutil, json, urllib, urllib2, BaseHTTPServer

# Fix issues with decoding HTTP responses
reload(sys)
sys.setdefaultencoding('utf8')

here = os.path.dirname(os.path.realpath(__file__))

roles = {}

def gen_response_payload(code=0, msg=''):
    return {'code': code, 'message': msg};

def get_roles(handler):
    return 0, roles

def get_role(handler):
    key = urllib.unquote(handler.path[7:])
    print key;
    if key == '':
        return get_roles(handler);
    elif key not in roles:
        return 1, gen_response_payload(1, "Unknown role: " + key);
    else:
        return 0, roles[key];

def add_role(handler):
    code = 0;
    msg = '';
    try:
        payload = handler.get_payload()
        role = payload['name'];
        if not isinstance(role, basestring):
            raise ValueError("name is not a string");
        
        weight = 1;
        if 'weight' in payload:
            weight = payload['weight'];
            if not isinstance(weight, int) and not isinstance(weight, float) and not isinstance(weight, float):
                raise ValueError("weight is not a float");
            weight = float(weight);
            if weight < 0:
                raise ValueError("weight is negative");
        
        if len(payload) != 2:
            code = 3;
            up = ''
            for x in payload:
                if x != 'name' and x != 'weight':
                    up += ' ' + x;
            msg = 'Unknown parameter(s): ' + up;
        elif role in roles:
            code = 3;
            msg = "Role exists";
        else:
            roles[role] = payload;
    except KeyError as error:
        code = 3;
        msg = str(error) + ' is missing';
    except ValueError as error:
        code = 3;
        msg = str(error);
    if code == 0:
        return 0, None;
    else:
        return code, gen_response_payload(code, msg);

def delete_role(handler):
    key = urllib.unquote(handler.path[7:])
    try:
        del roles[key]
        return 0, None;
    except KeyError:
        return 0, None;
    #return True # anything except None shows success

def get_weights(handler):
    return 0, roles
    #return None;

def get_weight(handler):
    key = urllib.unquote(handler.path[9:])
    print key;
    if key == '':
        return get_weights(handler);
    elif key not in roles:
        return 1, gen_response_payload(1, "Unknown role: " + key);
    else:
        return 0, roles[key];
    #return None;

def set_weight(handler):
    code = 0;
    msg = '';
    try:
        payload = handler.get_payload()
        role = payload['name'];
        if not isinstance(role, basestring):
            raise ValueError("name is not a string");
        weight = payload['weight'];
        if not isinstance(weight, int) and not isinstance(weight, float) and not isinstance(weight, float):
            raise ValueError("weight is not a float");
        weight = float(weight);
        if weight < 0:
            raise ValueError("weight is negative");
        if len(payload) != 2:
            code = 3;
            up = ''
            for x in payload:
                if x != 'name' and x != 'weight':
                    up += ' ' + x;
            msg = 'Unknown parameter(s): ' + up;
        elif role not in roles:
            code = 3;
            msg = "Role does not exist";
        else:
            roles[role] = payload;
    except KeyError as error:
        code = 3;
        msg = str(error) + ' is missing';
    except ValueError as error:
        code = 3;
        msg = str(error);
    if code == 0:
        return 0, None;
    else:
        return code, gen_response_payload(code, msg);
    #return None;

def rest_call_json(url, payload=None, with_payload_method='PUT'):
    'REST call with JSON decoding of the response and JSON payloads'
    if payload:
        if not isinstance(payload, basestring):
            payload = json.dumps(payload)
        # PUT or POST
        response = urllib2.urlopen(MethodRequest(url, payload, {'Content-Type': 'application/json'}, method=with_payload_method))
    else:
        # GET
        response = urllib2.urlopen(url)
    response = response.read().decode()
    return json.loads(response)

class MethodRequest(urllib2.Request):
    'See: https://gist.github.com/logic/2715756'
    def __init__(self, *args, **kwargs):
        if 'method' in kwargs:
            self._method = kwargs['method']
            del kwargs['method']
        else:
            self._method = None
        return urllib2.Request.__init__(self, *args, **kwargs)

    def get_method(self, *args, **kwargs):
        return self._method if self._method is not None else urllib2.Request.get_method(self, *args, **kwargs)

class RESTRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        self.routes = {
            r'^/$': {'file': 'web/index.html', 'media_type': 'text/html'},
            r'^/roles': {'GET': get_role, 'POST': add_role, 'DELETE': delete_role, 'media_type': 'application/json'},
            r'^/weights': {'GET': get_weight, 'POST': set_weight, 'media_type': 'application/json'}};
        
        return BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, *args, **kwargs)
    
    def do_HEAD(self):
        self.handle_method('HEAD')
    
    def do_GET(self):
        self.handle_method('GET')

    def do_POST(self):
        self.handle_method('POST')

    def do_PUT(self):
        self.handle_method('PUT')

    def do_DELETE(self):
        self.handle_method('DELETE')
    
    def get_payload(self):
        payload_len = int(self.headers.getheader('content-length', 0))
        payload = self.rfile.read(payload_len)
        payload = json.loads(payload)
        return payload
        
    def handle_method(self, method):
        route = self.get_route()
        if route is None:
            self.send_response(404)
            self.end_headers()
            self.wfile.write('Unknown endpoint\n')
        else:
            if method == 'HEAD':
                self.send_response(200)
                if 'media_type' in route:
                    self.send_header('Content-type', route['media_type'])
                self.end_headers()
            else:
                if 'file' in route:
                    if method == 'GET':
                        try:
                            f = open(os.path.join(here, route['file']))
                            try:
                                self.send_response(200)
                                if 'media_type' in route:
                                    self.send_header('Content-type', route['media_type'])
                                self.end_headers()
                                shutil.copyfileobj(f, self.wfile)
                            finally:
                                f.close()
                        except:
                            self.send_response(404)
                            self.end_headers()
                            self.wfile.write('File not found\n')
                    else:
                        self.send_response(405)
                        self.end_headers()
                        self.wfile.write('Only GET is supported\n')
                else:
                    if method in route:
                        code, content = route[method](self)
                        if code is not None:
                            if code == 0:
                                self.send_response(200)
                            else:
                                self.send_response(400)
                            if 'media_type' in route:
                                self.send_header('Content-type', route['media_type'])
                            self.end_headers()
                            if content is not None:
                                self.wfile.write(json.dumps(content))
                        else:
                            self.send_response(404)
                            self.end_headers()
                            self.wfile.write('Not found\n')
                    else:
                        self.send_response(405)
                        self.end_headers()
                        self.wfile.write(method + ' is not supported\n')
                    
    
    def get_route(self):
        for path, route in self.routes.iteritems():
            if re.match(path, self.path):
                return route
        return None

def rest_server(port):
    'Starts the REST server'
    http_server = BaseHTTPServer.HTTPServer(('', port), RESTRequestHandler)
    print 'Starting HTTP server at port %d' % port
    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        pass
    print 'Stopping HTTP server'
    http_server.server_close()

def main(argv):
    rest_server(12345)

if __name__ == '__main__':
    main(sys.argv[1:])
