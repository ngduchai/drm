namespace py drm
namespace cpp drm

typedef i32 Integer
typedef i64 Long
typedef double Double
typedef bool Bool
typedef string String

enum OperationType {
	LAUNCH = 1,
	UNKNOWN = 2
}

enum SignalType {
	KILL = 1,
	FORCEKILL = 2,
	SUSPEND = 3,
	RESUME = 4
}

struct ScalarResource {
	1: required Double scalar
}

struct RangeResource {
	1: required Integer rangeBegin,
	2: required Integer rangeEnd
}

struct Resource {
	1: required String agent,
	2: required String pool,
	3: optional ScalarResource cpu,
	4: optional ScalarResource gpu,
	5: optional ScalarResource mem,
	6: optional ScalarResource disk,
	7: optional RangeResource port
}

struct Agent {
	1: required String id,
	2: required String hostname,
	3: required String ip,
	4: required Integer port,
	5: optional Resource resources
}

struct ErrorInfo {
	1: required Integer code,
	2: required String message
}

exception DRMException {
	1: required ErrorInfo info
}

struct Response {
	1: required ErrorInfo info
}

struct PoolWeight {
	1: required String id,
	2: required Integer weight
}

struct Offer {
	1: required String id,
	2: required Agent agent,
	3: required Resource resources,
	4: required String poolId
}

struct Variable {
	1: required String name,
	2: required String value
}

struct Command {
	1: required String cmd,
	2: optional set<Variable> environment
}

struct TaskInfo {
	1: required String name,
	2: required String id,
	3: required String agentId,
	4: required set<Resource> resources,
	5: required Command cmd
}

struct Operation {
	1: required OperationType type,
	2: optional TaskInfo task
}

struct Filter {
	1: required Double refuse_seconds,
}

struct SecurityCredential {
	1: required String username,
	2: required String accessKey
}

service ResourcePartitioner {
	Response connect(1: SecurityCredential credential);
	Response disconnect(1: SecurityCredential credential);
	Response setWeight(1: set<PoolWeight> poolWeights);
	set<PoolWeight> getWeight();
}

service SystemAdmin {
	Response addResource(1: set<Resource> resources);
	Response removeResource(1: set<Resource> resources, 2: Integer timestamp, 3: Integer time);
	Response restart(1: Integer timestamp, 2: Integer time);
	Response shutdown(1: Integer timestamp, 2: Integer time);
}

service ResourcePoolOwner {
	Response connect(1: SecurityCredential credential);
	Response disconnect(1: SecurityCredential credential);
	// callback: void processOffer(1: set<Offer> offers);
	Response acceptOffer(1: Offer offer, 2: Resource resources, 3: Operation opt, 4: Filter filter);
	Response declineOffer(1: Offer offer, 2: Filter filter);
	Response releaseResource(1: set<Resource> resources);
	// callback: void processReclaimResource(1: Resource resources, 2: Integer timestamp, 3: Integer time);
	Response launchTask(1: String agentId, 2: TaskInfo task);
}

service PerformanceStatistics {
	Resource retrievePoolAllocation(1: String poolId) throws (1: DRMException e);
	set<Resource> retrieveAllPoolsAllocation();
}

service ResourcePartitionerCb {
	void onPoolChange(1: set<String> newPools, 2: set<String> unchangedPools, 3: set<String> disconnectedPools);
}



