/**
 * Autogenerated by Thrift
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#pragma once

#include "ResourcePoolOwnerInterface.h"

#include <folly/io/IOBuf.h>
#include <folly/io/IOBufQueue.h>
#include <thrift/lib/cpp/TApplicationException.h>
#include <thrift/lib/cpp/transport/THeader.h>
#include <thrift/lib/cpp2/GeneratedCodeHelper.h>
#include <thrift/lib/cpp2/GeneratedSerializationCodeHelper.h>
#include <thrift/lib/cpp2/server/Cpp2ConnContext.h>

namespace drm { namespace cpp2 {
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::SecurityCredential*>> ResourcePoolOwnerInterface_connect_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_connect_presult;
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::SecurityCredential*>> ResourcePoolOwnerInterface_disconnect_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_disconnect_presult;
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Offer*>, apache::thrift::FieldData<2, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Resource*>, apache::thrift::FieldData<3, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Operation*>, apache::thrift::FieldData<4, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Filter*>> ResourcePoolOwnerInterface_acceptOffer_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_acceptOffer_presult;
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Offer*>, apache::thrift::FieldData<2, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Filter*>> ResourcePoolOwnerInterface_declineOffer_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_declineOffer_presult;
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_SET, std::set< ::drm::cpp2::Resource>*>> ResourcePoolOwnerInterface_releaseResource_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_releaseResource_presult;
typedef apache::thrift::ThriftPresult<false, apache::thrift::FieldData<1, apache::thrift::protocol::T_STRING,  ::drm::cpp2::String*>, apache::thrift::FieldData<2, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::TaskInfo*>> ResourcePoolOwnerInterface_launchTask_pargs;
typedef apache::thrift::ThriftPresult<true, apache::thrift::FieldData<0, apache::thrift::protocol::T_STRUCT,  ::drm::cpp2::Response*>> ResourcePoolOwnerInterface_launchTask_presult;
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_connect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_connect<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_connect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_connect_pargs args;
  auto uarg_credential = std::make_unique< ::drm::cpp2::SecurityCredential>();
  args.get<0>().value = uarg_credential.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.connect", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function connect";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("connect", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function connect";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_connect<ProtocolIn_,ProtocolOut_>, throw_wrapped_connect<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_connect(std::move(callback), std::move(uarg_credential));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_connect(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_connect_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("connect", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_connect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function connect";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("connect", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function connect";
    }
  }
}

template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_disconnect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_disconnect<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_disconnect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_disconnect_pargs args;
  auto uarg_credential = std::make_unique< ::drm::cpp2::SecurityCredential>();
  args.get<0>().value = uarg_credential.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.disconnect", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function disconnect";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("disconnect", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function disconnect";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_disconnect<ProtocolIn_,ProtocolOut_>, throw_wrapped_disconnect<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_disconnect(std::move(callback), std::move(uarg_credential));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_disconnect(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_disconnect_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("disconnect", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_disconnect(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function disconnect";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("disconnect", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function disconnect";
    }
  }
}

template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_acceptOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_acceptOffer<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_acceptOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_acceptOffer_pargs args;
  auto uarg_offer = std::make_unique< ::drm::cpp2::Offer>();
  args.get<0>().value = uarg_offer.get();
  auto uarg_resources = std::make_unique< ::drm::cpp2::Resource>();
  args.get<1>().value = uarg_resources.get();
  auto uarg_opt = std::make_unique< ::drm::cpp2::Operation>();
  args.get<2>().value = uarg_opt.get();
  auto uarg_filter = std::make_unique< ::drm::cpp2::Filter>();
  args.get<3>().value = uarg_filter.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.acceptOffer", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function acceptOffer";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("acceptOffer", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function acceptOffer";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_acceptOffer<ProtocolIn_,ProtocolOut_>, throw_wrapped_acceptOffer<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_acceptOffer(std::move(callback), std::move(uarg_offer), std::move(uarg_resources), std::move(uarg_opt), std::move(uarg_filter));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_acceptOffer(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_acceptOffer_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("acceptOffer", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_acceptOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function acceptOffer";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("acceptOffer", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function acceptOffer";
    }
  }
}

template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_declineOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_declineOffer<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_declineOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_declineOffer_pargs args;
  auto uarg_offer = std::make_unique< ::drm::cpp2::Offer>();
  args.get<0>().value = uarg_offer.get();
  auto uarg_filter = std::make_unique< ::drm::cpp2::Filter>();
  args.get<1>().value = uarg_filter.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.declineOffer", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function declineOffer";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("declineOffer", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function declineOffer";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_declineOffer<ProtocolIn_,ProtocolOut_>, throw_wrapped_declineOffer<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_declineOffer(std::move(callback), std::move(uarg_offer), std::move(uarg_filter));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_declineOffer(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_declineOffer_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("declineOffer", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_declineOffer(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function declineOffer";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("declineOffer", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function declineOffer";
    }
  }
}

template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_releaseResource(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_releaseResource<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_releaseResource(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_releaseResource_pargs args;
  auto uarg_resources = std::make_unique<std::set< ::drm::cpp2::Resource>>();
  args.get<0>().value = uarg_resources.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.releaseResource", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function releaseResource";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("releaseResource", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function releaseResource";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_releaseResource<ProtocolIn_,ProtocolOut_>, throw_wrapped_releaseResource<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_releaseResource(std::move(callback), std::move(uarg_resources));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_releaseResource(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_releaseResource_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("releaseResource", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_releaseResource(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function releaseResource";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("releaseResource", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function releaseResource";
    }
  }
}

template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::_processInThread_launchTask(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot, apache::thrift::Cpp2RequestContext* ctx, folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  auto pri = iface_->getRequestPriority(ctx, apache::thrift::concurrency::NORMAL);
  processInThread<ProtocolIn_, ProtocolOut_>(std::move(req), std::move(buf),std::move(iprot), ctx, eb, tm, pri, apache::thrift::RpcKind::SINGLE_REQUEST_SINGLE_RESPONSE, &ResourcePoolOwnerInterfaceAsyncProcessor::process_launchTask<ProtocolIn_, ProtocolOut_>, this);
}
template <typename ProtocolIn_, typename ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::process_launchTask(std::unique_ptr<apache::thrift::ResponseChannel::Request> req, std::unique_ptr<folly::IOBuf> buf, std::unique_ptr<ProtocolIn_> iprot,apache::thrift::Cpp2RequestContext* ctx,folly::EventBase* eb, apache::thrift::concurrency::ThreadManager* tm) {
  // make sure getConnectionContext is null
  // so async calls don't accidentally use it
  iface_->setConnectionContext(nullptr);
  ResourcePoolOwnerInterface_launchTask_pargs args;
  auto uarg_agentId = std::make_unique< ::drm::cpp2::String>();
  args.get<0>().value = uarg_agentId.get();
  auto uarg_task = std::make_unique< ::drm::cpp2::TaskInfo>();
  args.get<1>().value = uarg_task.get();
  std::unique_ptr<apache::thrift::ContextStack> ctxStack(this->getContextStack(this->getServiceName(), "ResourcePoolOwnerInterface.launchTask", ctx));
  try {
    deserializeRequest(args, buf.get(), iprot.get(), ctxStack.get());
  }
  catch (const std::exception& ex) {
    ProtocolOut_ prot;
    if (req) {
      LOG(ERROR) << ex.what() << " in function launchTask";
      apache::thrift::TApplicationException x(apache::thrift::TApplicationException::TApplicationExceptionType::PROTOCOL_ERROR, ex.what());
      folly::IOBufQueue queue = serializeException("launchTask", &prot, ctx->getProtoSeqId(), nullptr, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), ctx->getHeader()->getWriteTransforms(), ctx->getHeader()->getMinCompressBytes()));
      eb->runInEventBaseThread([queue = std::move(queue), req = std::move(req)]() mutable {
        if (req->isStream()) {
          req->sendStreamReply({queue.move(), {}});
        } else {
          req->sendReply(queue.move());
        }
      }
      );
      return;
    }
    else {
      LOG(ERROR) << ex.what() << " in oneway function launchTask";
    }
  }
  auto callback = std::make_unique<apache::thrift::HandlerCallback<std::unique_ptr< ::drm::cpp2::Response>>>(std::move(req), std::move(ctxStack), return_launchTask<ProtocolIn_,ProtocolOut_>, throw_wrapped_launchTask<ProtocolIn_, ProtocolOut_>, ctx->getProtoSeqId(), eb, tm, ctx);
  if (!callback->isRequestActive()) {
    callback.release()->deleteInThread();
    return;
  }
  ctx->setStartedProcessing();
  iface_->async_tm_launchTask(std::move(callback), std::move(uarg_agentId), std::move(uarg_task));
}

template <class ProtocolIn_, class ProtocolOut_>
folly::IOBufQueue ResourcePoolOwnerInterfaceAsyncProcessor::return_launchTask(int32_t protoSeqId, apache::thrift::ContextStack* ctx,  ::drm::cpp2::Response const& _return) {
  ProtocolOut_ prot;
  ResourcePoolOwnerInterface_launchTask_presult result;
  result.get<0>().value = const_cast< ::drm::cpp2::Response*>(&_return);
  result.setIsSet(0, true);
  return serializeResponse("launchTask", &prot, protoSeqId, ctx, result);
}

template <class ProtocolIn_, class ProtocolOut_>
void ResourcePoolOwnerInterfaceAsyncProcessor::throw_wrapped_launchTask(std::unique_ptr<apache::thrift::ResponseChannel::Request> req,int32_t protoSeqId,apache::thrift::ContextStack* ctx,folly::exception_wrapper ew,apache::thrift::Cpp2RequestContext* reqCtx) {
  if (!ew) {
    return;
  }
  ProtocolOut_ prot;
   {
    if (req) {
      LOG(ERROR) << ew << " in function launchTask";
      apache::thrift::TApplicationException x(ew.what().toStdString());
      ctx->userExceptionWrapped(false, ew);
      ctx->handlerErrorWrapped(ew);
      folly::IOBufQueue queue = serializeException("launchTask", &prot, protoSeqId, ctx, x);
      queue.append(apache::thrift::transport::THeader::transform(queue.move(), reqCtx->getHeader()->getWriteTransforms(), reqCtx->getHeader()->getMinCompressBytes()));
      req->sendReply(queue.move());
      return;
    }
    else {
      LOG(ERROR) << ew << " in oneway function launchTask";
    }
  }
}

}} // drm::cpp2
namespace apache { namespace thrift {

}} // apache::thrift
