
import sys;

import matplotlib;
matplotlib.use('Agg')

import matplotlib.pyplot as plt;

def gettime(time):
	token = time.split(":");
	time = float(token[2]);
	time += float(token[1]) * 60;
	time += float(token[0]) * 3600;
	return time;

cpus_plot = {};
mem_plot = {};
disk_plot = {};
time_plot = {};
roles = {};

def update(role, time, cpus, mem, disk):
	if (role not in roles):
		roles[role] = role;
		cpus_plot[role] = [];
		mem_plot[role] = [];
		disk_plot[role] = [];
		time_plot[role] = [];
	cpus_plot[role].append(cpus);
	mem_plot[role].append(mem / 1024);
	disk_plot[role].append(disk / 1024);
	time_plot[role].append(time);

# Read data

path = sys.argv[1];
with open(path) as f:
	stime = -1;
	for line in f:
		tokens = line.split();
		time = gettime(tokens[1]);
		if stime == -1:
			stime = time;
		time -= stime;
		role = tokens[5];
		cpus = 0;
		mem = 0;
		disk = 0;
		i = 6;
		while i < len(tokens):
			if "cpus" in tokens[i]:
				cpus = float(tokens[i].split(":")[1].split(";")[0]);
			elif "mem" in tokens[i]:
				mem = float(tokens[i].split(":")[1].split(";")[0]);
			elif "disk" in tokens[i]:
				disk = float(tokens[i].split(":")[1].split(";")[0]);
			i = i + 1;
		update(role, time, cpus, mem, disk);

# Draw graphs
plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], cpus_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("No. CPU");
plt.legend(loc=1);
plt.title("CPU Allocation");
plt.ylim([-1, 100]);
plt.xlim([0, 2500]);
plt.savefig("cpu_usage.pdf", bbox_inches='tight');
plt.close();

plt.figure(0);
l = 0;
for role in roles:
	if len(cpus_plot[role]) > l:
		l = len(cpus_plot[role]);
sum_cpus = [0] * l;
for role in roles:
	i = 0;
	if len(cpus_plot[role]) < l:
		i = l - len(cpus_plot[role]);
	for j in xrange(len(cpus_plot[role])):
		sum_cpus[i+j] += cpus_plot[role][j];
#for i in sum_cpus:
#	i = i / 96.0 * 100.0;
for role in roles:
	if len(time_plot[role]) < l:
		continue;
	plt.plot(time_plot[role], sum_cpus, label="Total usage");
	break;

plt.xlabel("Time (second)");
plt.ylabel("No. CPU");
plt.legend(loc=4);
plt.title("Total CPU Utilization");
plt.ylim([-1, 101]);
plt.xlim([0, 2500]);
plt.savefig("sum_cpu_usage.pdf", bbox_inches='tight');
plt.close();



plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], mem_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("Memory (GB)");
plt.legend(loc=1);
plt.title("Memory Allocation");
plt.ylim([-1, 1100]);
plt.xlim([0, 2500]);
plt.savefig("mem_usage.pdf", bbox_inches='tight');
plt.close();

plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], disk_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("Disk (GB)");
plt.legend(loc=1);
plt.title("Disk Allocation");
plt.ylim([-1, 100000]);
plt.xlim([0, 2500]);
plt.savefig("disk_usage.pdf", bbox_inches='tight');
plt.close();











