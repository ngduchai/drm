#!/bin/bash

MESOS_PATH=/home/ndhai/home/sources/mesos-1.6.0/build/
CUSTOM_PATH=$MESOS_PATH/custom

sbatch start-master.sbatch
sleep 10
for value in {1..8}
do
	sbatch start-agents.sbatch
	sleep 1
done

# Spark frameworks
#sbatch start-spark-1.sbatch
#sleep 2
#sbatch start-spark-2.sbatch
#sleep 2
#sbatch start-spark-3.sbatch
#sleep 2
#sbatch start-spark-4.sbatch
#sleep 20
#sbatch submit-spark-1.sbatch
#sbatch submit-spark-2.sbatch
#sbatch submit-spark-3.sbatch
#sbatch submit-spark-4.sbatch

sleep 10

srun curl -d @$CUSTOM_PATH/weights/init.json -X PUT http://river-110:5050/weights

sleep 10

# Test framework
sbatch start-test-framework-1.sbatch
sbatch start-test-framework-2.sbatch
sbatch start-test-framework-3.sbatch
sbatch start-test-framework-4.sbatch

sleep 500

#read -p "Next"

srun curl -d @$CUSTOM_PATH/weights/change1.json -X PUT http://river-110:5050/weights

sleep 500

sbatch start-test-framework-5.sbatch
srun curl -d @$CUSTOM_PATH/weights/add5.json -X PUT http://river-110:5050/weights

sleep 500

srun curl -d @$CUSTOM_PATH/weights/change23.json -X PUT http://river-110:5050/weights


