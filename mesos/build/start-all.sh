#!/bin/bash
sbatch start-master.sbatch
for value in {1..8}
do
	sbatch start-agents.sbatch
	sleep 1
done

# Spark frameworks
#sbatch start-spark-1.sbatch
#sleep 2
#sbatch start-spark-2.sbatch
#sleep 2
#sbatch start-spark-3.sbatch
#sleep 2
#sbatch start-spark-4.sbatch
#sleep 20
#sbatch submit-spark-1.sbatch
#sbatch submit-spark-2.sbatch
#sbatch submit-spark-3.sbatch
#sbatch submit-spark-4.sbatch

CUSTOM_PATH=/home/ndhai/home/sources/mesos-1.6.0/build/custom
srun curl -d @$CUSTOM_PATH/weights/init.json -X PUT http://river-110:5050/weights

sleep 10

# Test framework
sbatch start-test-framework-1.sbatch
sbatch start-test-framework-2.sbatch
sbatch start-test-framework-3.sbatch
#sleep 200
sbatch start-test-framework-4.sbatch



