
import sys;

import matplotlib;
matplotlib.use('Agg')

import matplotlib.pyplot as plt;

def gettime(time):
	token = time.split(":");
	time = float(token[2]);
	time += float(token[1]) * 60;
	time += float(token[0]) * 3600;
	return time;

cpus_plot = {};
mem_plot = {};
disk_plot = {};
time_plot = {};
roles = {};

def update(role, time, cpus, mem, disk):
	if (role not in roles):
		roles[role] = role;
		cpus_plot[role] = [];
		mem_plot[role] = [];
		disk_plot[role] = [];
		time_plot[role] = [];
	if cpus == 0 or mem == 0 or disk == 0:
		return;
	cpus_plot[role].append(cpus);
	mem_plot[role].append(mem);
	disk_plot[role].append(disk);
	time_plot[role].append(time);

# Read data

path = sys.argv[1];
with open(path) as f:
	stime = -1;
	for line in f:
		tokens = line.split();
		time = gettime(tokens[1]);
		if stime == -1:
			stime = time;
		time -= stime;
		role = tokens[5];
		cpus = 0;
		mem = 0;
		disk = 0;
		i = 6;
		while i < len(tokens):
			if "cpus" in tokens[i]:
				cpus = float(tokens[i].split(":")[1].replace(";", ""));
			elif "mem" in tokens[i]:
				nm = tokens[i].split(":")[1];
				if "GB" in nm:
					mem = float(nm.replace("GB", ""));
				elif "MB" in nm:
					mem = float(nm.replace("MB", "")) / 1024;
				elif "B" in nm:
					mem = float(nm.replace("B", "")) * 1024 * 1024;
				else:
					print nm;
					exit(1);
			elif "disk" in tokens[i]:
				nd = tokens[i].split(":")[1];
				if "GB" in nd:
					disk = float(nd.replace("GB", ""));
				elif "MB" in nd:
					disk = float(nd.replace("MB", "")) / 1024;
				elif "B" in nd:
					disk = float(nd.replace("B", "")) * 1024 * 1024;
				else:
					print nd;
					exit(1);
			i = i + 1;
		update(role, time, cpus, mem, disk);

# Draw graphs
plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], cpus_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("No. CPU");
plt.legend(loc=1);
plt.title("Offered CPU");
plt.ylim([-1, 100]);
#plt.ylim([-1, 20]);
#plt.xlim([0, 3000]);
plt.savefig("cpu_offered.pdf", bbox_inches='tight');
plt.close();

plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], mem_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("Memory (GB)");
plt.legend(loc=1);
plt.title("Offered Memory");
plt.ylim([-1, 1100]);
plt.xlim([0, 3000]);
plt.savefig("mem_offered.pdf", bbox_inches='tight');
plt.close();

plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], disk_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("Disk (GB)");
plt.legend(loc=1);
plt.title("Offered Disk");
plt.ylim([-1, 100000]);
plt.xlim([0, 3000]);
plt.savefig("disk_offered.pdf", bbox_inches='tight');
plt.close();








