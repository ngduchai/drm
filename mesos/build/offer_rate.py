
import sys;

import matplotlib;
matplotlib.use('Agg')

import matplotlib.pyplot as plt;

def gettime(time):
	token = time.split(":");
	time = float(token[2]);
	time += float(token[1]) * 60;
	time += float(token[0]) * 3600;
	return time;

rate_plot = {};
time_plot = {};
roles = {};

def update(role, time, rate):
	if (role not in roles):
		roles[role] = role;
		rate_plot[role] = [];
		time_plot[role] = [];
	rate_plot[role].append(rate);
	time_plot[role].append(time);

# Read data

path = sys.argv[1];
with open(path) as f:
	stime = -1;
	ptime = -1;
	count = 0;
	for line in f:
		tokens = line.split();
		time = gettime(tokens[1]);
		if stime == -1:
			stime = time;
			ptime = time;
			continue;
		count = count + 1;
		if time - ptime > 5:
			rate = float(count) / float(time - ptime);
			print ptime - stime, rate;
			update("All", ptime - stime, rate);
			count = 0;
			ptime = time;
		

# Draw graphs
plt.figure(0);
for role in roles:
	plt.plot(time_plot[role], rate_plot[role], label=role);
plt.xlabel("Time (second)");
plt.ylabel("Offer rate (offer/second)");
plt.legend(loc=1);
plt.title("Offer rate");
#plt.ylim([-1, 100]);
#plt.ylim([-1, 20]);
#plt.xlim([0, 3000]);
plt.savefig("offer_rate.pdf", bbox_inches='tight');
plt.close();







