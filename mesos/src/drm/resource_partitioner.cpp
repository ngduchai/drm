
#include "resource_partitioner.hpp"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/stdcxx.h>

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
//using namespace mesos::internal::master;
using namespace drm;

ResourcePartitioner::ResourcePartitioner(std::string drmHostname, int port) {
	this->initialize(drmHostname, port);
}

void ResourcePartitioner::initialize(std::string drmHostname, int drmPort) {
	/* connect to DRM */
	stdcxx::shared_ptr<TTransport> socket(new TSocket(drmHostname, drmPort));
	stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
	stdcxx::shared_ptr<TProtocol> rpProtocol(new TBinaryProtocol(transport));
  	stdcxx::shared_ptr<TProtocol> protocol(new TMultiplexedProtocol(
		rpProtocol, "Resource Partitioner"));

	_client = new ResourcePartitionerClient(protocol);
	
	transport->open();
	
}

void ResourcePartitioner::finalize() {
	if (_client != nullptr) {
		delete _client;
		_client = nullptr;
	}
}

void ResourcePartitioner::initCallback(Response& _return,
		const SecurityCredential& credential) {
	stdcxx::shared_ptr<ResourcePartitioner::Handler> handler(new
		ResourcePartitioner::Handler(this));
	/* open connect for receiving DRM call back */
	stdcxx::shared_ptr<TProcessor> processor(new ResourcePartitionerCbProcessor(handler));
	//stdcxx::shared_ptr<TServerSocket> serversocket(new TServerSocket(0));
	stdcxx::shared_ptr<TServerSocket> serversocket(new TServerSocket(22222));
	stdcxx::shared_ptr<TTransportFactory> transport(new TBufferedTransportFactory());
	stdcxx::shared_ptr<TProtocolFactory> protocol(new TBinaryProtocolFactory());
	_server = new TSimpleServer(processor, serversocket, transport, protocol);
	/* Inform DRM about the information of the call back receiver */
	int port = serversocket->getPort();
	char hn[1024];
	gethostname(hn, 1024);
	std::string hostname(hn);
	_cbthread = new std::thread(&ResourcePartitioner::_startcb, this);
	//_server->run();
	_client->updateConnection(_return, credential, hostname, port);
}

void ResourcePartitioner::getWeight(std::set<PoolWeight> & _return) {
	return _client->getWeight(_return);
}

void ResourcePartitioner::connect(Response& _return, const SecurityCredential& credential) {
	_client->connect(_return, credential);
	if (_return.info.code == 0) {
		initCallback(_return, credential);
	}
}

void ResourcePartitioner::disconnect(Response& _return,
		const SecurityCredential& credential) {
	_client->disconnect(_return, credential);
	if (_return.info.code == 0) {
		_server->stop();
		_cbthread->join();
		delete _server;
		_server = nullptr;
		delete _cbthread;
		_cbthread = nullptr;
	}
}

void ResourcePartitioner::setWeight(Response& _return,
		const std::set<PoolWeight> & poolWeights) {
	return _client->setWeight(_return, poolWeights);
}

void ResourcePartitioner::_startcb() {
	if (_server != nullptr) {
		_server->serve();
	}
}

ResourcePartitioner::Handler::Handler(ResourcePartitioner *rpIf) : _rpIf(rpIf) {
	
};			



