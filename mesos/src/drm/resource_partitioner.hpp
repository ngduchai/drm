
#ifndef RESOURCE_PARTITIONER_H
#define RESOURCE_PARTITIONER_H

#include <thread>
#include <string>


#include <thrift/server/TSimpleServer.h>

#include "gen-cpp/ResourcePartitioner.h"
#include "gen-cpp/ResourcePartitionerCb.h"

namespace drm {

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

class ResourcePartitioner {
public:
	ResourcePartitioner(std::string drmHostname, int port);
	void initialize(std::string drmHostname, int drmPort);
	void finalize();
	
	void getWeight(std::set<PoolWeight> & _return);
	void connect(Response& _return, const SecurityCredential& credential);
	void disconnect(Response& _return, const SecurityCredential& credential);
	void setWeight(Response& _return, const std::set<PoolWeight> & poolWeights);
				
	// call back implemented by user
	virtual void onPoolChange(const std::set<PoolInfo> & newPools,
		const std::set<PoolInfo> & unchangedPools,
		const std::set<PoolInfo> & disconnectedPools) = 0;
protected:
	class Handler : public ResourcePartitionerCbIf { 
	public:
		Handler(ResourcePartitioner *rpIf);
		virtual void onPoolChange(const std::set<PoolInfo> & newPools,
				const std::set<PoolInfo> & unchangedPools,
				const std::set<PoolInfo> & disconnectedPools) {
			/*
			std::thread t(&ResourcePartitioner::onPoolChange, _rpIf,
				newPools, unchangedPools, disconnectedPools);
			*/
			_rpIf->onPoolChange(newPools, unchangedPools, disconnectedPools);
		}
	private:
		ResourcePartitioner *_rpIf = nullptr;
	};
	bool _connected = false;
	ResourcePartitionerClient * _client = nullptr;
	std::thread * _cbthread = nullptr;
	TServer * _server = nullptr;
	void _startcb();
	virtual void initCallback(Response& _return, const SecurityCredential& credential);
};

}

#endif


