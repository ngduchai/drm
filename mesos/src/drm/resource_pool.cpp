
#include "resource_pool.hpp"

#include <iostream>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/stdcxx.h>

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
using namespace drm;

ResourcePool::ResourcePool(std::string name) {
	_info.name = name;
}

ResourcePool::ResourcePool(std::string name, std::string drmHostname, int port) :
		ResourcePool(name) {
	this->initialize(drmHostname, port);
}

void ResourcePool::initialize(std::string drmHostname, int drmPort) {
	/* connect to DRM */
	stdcxx::shared_ptr<TTransport> socket(new TSocket(drmHostname, drmPort));
	stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
	stdcxx::shared_ptr<TProtocol> rpProtocol(new TBinaryProtocol(transport));
  	stdcxx::shared_ptr<TProtocol> protocol(new TMultiplexedProtocol(
		rpProtocol, "Resource Pool"));
	
	_client = new ResourcePoolClient(protocol);
	
	transport->open();
	
}

void ResourcePool::finalize() {
	if (_client != nullptr) {
		delete _client;
		_client = nullptr;
	}
}

void ResourcePool::initCallback(Response& _return,
		const SecurityCredential& credential) {
	std::cout << "start" << std::endl;
	stdcxx::shared_ptr<ResourcePool::Handler> handler(new ResourcePool::Handler(this));
	/* open connect for receiving DRM call back */
	stdcxx::shared_ptr<TProcessor> processor(new ResourcePoolCbProcessor(handler));
	//stdcxx::shared_ptr<TServerSocket> serversocket(new TServerSocket(0));
	stdcxx::shared_ptr<TServerSocket> serversocket(new TServerSocket(11111));
	stdcxx::shared_ptr<TTransportFactory> transport(new TBufferedTransportFactory());
	stdcxx::shared_ptr<TProtocolFactory> protocol(new TBinaryProtocolFactory());
	_server = new TSimpleServer(processor, serversocket, transport, protocol);
	/* Inform DRM about the information of the call back receiver */
	int port = serversocket->getPort();
	char hn[1024];
	gethostname(hn, 1024);
	std::string hostname(hn);
	_cbthread = new std::thread(&ResourcePool::_startcb, this);
	//_server->run();
	_client->updateConnection(_return, _info.id, credential, hostname, port);
}

void ResourcePool::connect(Response& ret, const SecurityCredential& credential) {
	_client->connect(ret, _info, credential);
	if (ret.info.code == 0) {
		_info.id = ret.info.message;
		initCallback(ret, credential);
		std::cout << "connected" << std::endl;
	}

}
void ResourcePool::disconnect(Response& ret, const SecurityCredential& credential) {
	_client->disconnect(ret, _info.id, credential);
	if (ret.info.code == 0) {
		_server->stop();
		_cbthread->join();
		delete _server;
		_server = nullptr;
		delete _cbthread;
		_cbthread = nullptr;
	}
}

void ResourcePool::acceptOffer(Response& ret, const Offer& offer, const Resource& resources,
		const Operation& opt, const Filter& filter) {
	return _client->acceptOffer(ret, offer, resources, opt, filter);
}

void ResourcePool::declineOffer(Response& ret, const Offer& offer, const Filter& filter) {
	return _client->declineOffer(ret, offer, filter);
}

void ResourcePool::releaseResource(Response& ret, const std::set<Resource>& resources) {
	return _client->releaseResource(ret, resources);
}

void ResourcePool::launchTask(Response& ret, const String& agentId, const TaskInfo& task) {
	return _client->launchTask(ret, agentId, task);
}

void ResourcePool::_startcb() {
	if (_server != nullptr) {
		_server->serve();
	}
}

ResourcePool::Handler::Handler(ResourcePool *rpIf) : _rpIf(rpIf) {
	
};			






