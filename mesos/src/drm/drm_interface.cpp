
#include "drm/../master/master.hpp"
#include "drm/drm_interface.hpp"

#include "process/protobuf.hpp"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
using namespace mesos;
using namespace mesos::internal;
using namespace mesos::internal::master;
using namespace drm;

void updateResponse(Response& res, int code, std::string message) {
	res.info.code = code;
	res.info.message = message;
}

bool inline noError(Response& res) { return (res.info.code == 0); }

void ResourcePartitionerHandler::getWeight(std::set<PoolWeight> & _return) {
  mesos::allocator::Allocator * allocator = _master->getAllocator();
  for (auto pool : allocator->getWeights()) {
    PoolWeight temp;
    temp.id = pool.first;
    temp.weight = pool.second;
    _return.insert(temp);
  }
}

void ResourcePartitionerHandler::connect(Response& _return,
		const SecurityCredential& credential) {
	if (_cb == nullptr) {
		// no resource partitioner currently connects
		updateResponse(_return, 0, "success");
	}else{
		updateResponse(_return, 4, "Another resource paritioner is currently connecting");
	}
}

void ResourcePartitionerHandler::disconnect(Response& _return,
		const SecurityCredential& credential) {
	if (_cb != nullptr) {
		delete _cb;
		_cb = nullptr;
	}
}

void ResourcePartitionerHandler::setWeight(Response& _return,
		const std::set<PoolWeight> & poolWeights) {
	updateResponse(_return, 0, "success");
	std::vector<WeightInfo> newWeights;
	for (auto weight : poolWeights) {
		if (weight.weight < 0) {
			updateResponse(_return, 1, "Weight of pool " + weight.id + " = " +
					std::to_string(weight.weight) + " < 0");
			break;
		}
		LOG(INFO) << weight.weight;
		WeightInfo info;
		info.set_role(weight.id);
		info.set_weight(weight.weight);
		newWeights.push_back(info);
	}

	if (noError(_return)) {
		_master->getAllocator()->updateWeights(newWeights);
	}
}


void ResourcePartitionerHandler::updateConnection(Response& _return,
		const SecurityCredential& credential,
		const String& hostname, Integer port) {
	
	if (_cb != nullptr) {
		delete _cb;
	}
	
	stdcxx::shared_ptr<TTransport> socket(new TSocket(hostname, port));
	stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
	stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
	_cb = new ResourcePartitionerCbClient(protocol);
	transport->open();
	_return.info.code = 0;
}

void ResourcePartitionerHandler::updatePools(const std::set<PoolInfo> & newPools,
		const std::set<PoolInfo> & unchangedPools,
		const std::set<PoolInfo> & disconnectedPools) {
	//try {
		if (_cb != nullptr) {
			_cb->onPoolChange(newPools, unchangedPools, disconnectedPools);
		}
	//} catch (...) {
	//	LOG(WARNING) << "Cannot update pool.";
	//}
}


bool ResourcePartitionerHandler::validCredential(SecurityCredential& credential) {
	return true;
}


void SystemAdminHandler::addResource(Response& response,
		const std::set<Resource>& resources) {
	
}

void SystemAdminHandler::removeResource(Response& response,
		const std::set<Resource>& resources, Integer timestamp, Integer time) {
	
}

void SystemAdminHandler::restart(Response& response,
		Integer timestamp, Integer time) {
	
}

void SystemAdminHandler::shutdown(Response& response,
		Integer timestamp, Integer time) {
	
}

#include <chrono>
void ResourcePoolHandler::connect(Response& ret, const PoolInfo& pool,
		const SecurityCredential& credential) {
	/* Add the pool as mesos framework */
	RegisterFrameworkMessage message;
	FrameworkInfo& framework = const_cast<FrameworkInfo&>(message.framework());
	LOG(INFO) << "Pool: " << pool.name;
	framework.set_name(pool.name);
	framework.set_user(credential.username);
	framework.set_role(pool.name);
	LOG(INFO) << "role: " << framework.role();
	
	process::UPID upid;
	_master->registerFramework(upid, std::move(message));
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	FrameworkID * fid = _master->getFrameworkID(pool.name);
	if (fid != nullptr) {
		PoolInfo poolinfo = pool;
		poolinfo.id = fid->value();
		_pools[poolinfo.id].info = poolinfo;
		_pools[poolinfo.id].cb = nullptr;
		LOG(INFO) << "New pool connected: " << pool.name <<  ". ID: " << poolinfo.id;
		updateResponse(ret, 0, poolinfo.id);
		// Let resource partitioner know of the changes
		set<PoolInfo> newPools;
		set<PoolInfo> unchangedPools;
		set<PoolInfo> deletedPools;
		newPools.insert(poolinfo);
		auto pools = _master->getAllocator()->getWeights();
		for (auto pool : pools) {
			if (pool.first == poolinfo.name) {
				continue;
			}
			PoolInfo info;
			info.id = _master->getFrameworkID(pool.first)->value();
			info.name = pool.first;
			unchangedPools.insert(info);
		}
		_master->drmint.updatePools(newPools, unchangedPools, deletedPools);
	}
}

void ResourcePoolHandler::disconnect(Response& ret, const String& poolId,
		const SecurityCredential& credential) {
	FrameworkID fid;
	fid.set_value(poolId);
	process::UPID upid;
	_master->unregisterFramework(upid, fid);
	if (_pools.find(poolId) == _pools.end()) {
		LOG(INFO) << "Cannot find pool id: " << poolId;
		updateResponse(ret, 4, "Pool ID not found");
		return;
	}
	updateResponse(ret, 0, "success");
	PoolInfo poolinfo = _pools[poolId].info;
	delete _pools[poolId].cb;
	_pools[poolId].cb = nullptr;
	_pools.erase(poolId);
	// Let resource partitioner know of the changes
	set<PoolInfo> newPools;
	set<PoolInfo> unchangedPools;
	set<PoolInfo> deletedPools;
	deletedPools.insert(poolinfo);
	auto pools = _master->getAllocator()->getWeights();
	for (auto pool : pools) {
		if (pool.first == poolinfo.name) {
			continue;
		}
		PoolInfo info;
		info.id = _master->getFrameworkID(pool.first)->value();
		info.name = pool.first;
		unchangedPools.insert(info);
	}
	LOG(INFO) << "Update changes";
	_master->drmint.updatePools(newPools, unchangedPools, deletedPools);
}

void ResourcePoolHandler::acceptOffer(Response& ret, const Offer& offer,
		const Resource& resources, const Operation& opt, const Filter& filter) {
	updateResponse(ret, 0, "received message");
	_master->acceptOffer(offer, resources, opt, filter);
}

void ResourcePoolHandler::declineOffer(Response& ret, const Offer& offer,
		const Filter& filter) {
	updateResponse(ret, 0, "received message");
	_master->declineOffer(offer, filter);

}

void ResourcePoolHandler::releaseResource(Response& ret,
		const std::set<Resource>& resources) {
	
}

void ResourcePoolHandler::launchTask(Response& ret, const String& agentId,
		const TaskInfo& task) {
	
}

void ResourcePoolHandler::updateConnection(Response& _return,
		const String& poolId,
		const SecurityCredential& credential,
		const String& hostname, Integer port) {
	
	stdcxx::shared_ptr<TTransport> socket(new TSocket(hostname, port));
	stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
	stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
	ResourcePoolCbClient * cb = new ResourcePoolCbClient(protocol);
	transport->open();
	_pools[poolId].cb = cb;
	_return.info.code = 0;
}

void ResourcePoolHandler::sendOffers(const std::set<Offer>& offers) {
	//std::string poolId = offers.begin()->resource.pool;
	std::string poolId = offers.begin()->poolId;
	int check = 6;
	while (check) {
		if (_pools.find(poolId) == _pools.end()) {
			LOG(ERROR) << "Pool with Id = " << poolId << " does not exist";
		}else if (_pools[poolId].cb == nullptr) {
			LOG(ERROR) << "No connection to pool with Id = " << poolId;
		}else{
			_pools[poolId].cb->processOffer(offers);
			LOG(INFO) << "Sent offers";
			break;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

bool ResourcePoolHandler::validCredential(SecurityCredential& credential) {
	return true;
}

void PerformanceStatisticsHandler::retrievePoolAllocation(Resource& ret,
		const String& poolId) {
	
}

void PerformanceStatisticsHandler::retrieveAllPoolsAllocation(
		std::set<Resource>& ret) {
	
}

void DrmInterface::initialize(int port) {
	stdcxx::shared_ptr<TMultiplexedProcessor> multiplexedProcessor(
		new TMultiplexedProcessor());
	stdcxx::shared_ptr<TProcessor> processor; 
	multiplexedProcessor->registerDefault(processor);
	/* Resource Partitioner */
	_rpHandler = stdcxx::make_shared<ResourcePartitionerHandler>(_master);
	stdcxx::shared_ptr<ResourcePartitionerProcessor> rpProcessor(
		new ResourcePartitionerProcessor(_rpHandler));
	multiplexedProcessor->registerProcessor("Resource Partitioner", rpProcessor);
	/* System Admin */
	_saHandler = stdcxx::make_shared<SystemAdminHandler>(_master);
	stdcxx::shared_ptr<SystemAdminProcessor> saProcessor(
		new SystemAdminProcessor(_saHandler));
	multiplexedProcessor->registerProcessor("System Admin", saProcessor);
	/* Resource Pool Owner */
	_rpoHandler = stdcxx::make_shared<ResourcePoolHandler>(_master);
	stdcxx::shared_ptr<ResourcePoolProcessor> rpoProcessor(
		new ResourcePoolProcessor(_rpoHandler));
	multiplexedProcessor->registerProcessor("Resource Pool", rpoProcessor);
	/* Performance Statistics */
	_psHandler = make_shared<PerformanceStatisticsHandler>(_master);
	stdcxx::shared_ptr<PerformanceStatisticsProcessor> psProcessor(
		new PerformanceStatisticsProcessor(_psHandler));
	multiplexedProcessor->registerProcessor("Performance Statistics", psProcessor);
	
	processor = stdcxx::dynamic_pointer_cast<TProcessor>(multiplexedProcessor);
	
	/* initialize server socket */
	stdcxx::shared_ptr<TServerSocket> serverSocket(new TServerSocket(port));
	
	/* initialize transport factory */
	stdcxx::shared_ptr<TTransportFactory> transportFactory(
		new TBufferedTransportFactory());
	
	/* initialize protocol factory */
	stdcxx::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
	
	/* initialize server */
	_server = new TThreadedServer(
		processor,
		serverSocket,
		transportFactory,
		protocolFactory
	);
	
	LOG(INFO) << "Starting the server...";
	//_server->run();
	serverthread = new std::thread(&drm::DrmInterface::_startint, this);
}

void DrmInterface::finalize() {
	if (serverthread != nullptr) {
		_server->stop();
		delete _server;
		_server = nullptr;
		serverthread->join();
		delete serverthread;
		serverthread = nullptr;
	}
}

void DrmInterface::_startint() {
	if (_server != nullptr) {
		_server->serve();
	}
}


void DrmInterface::updatePools(const std::set<PoolInfo> & newPools,
		const std::set<PoolInfo> & unchangedPools,
		const std::set<PoolInfo> & disconnectedPools) {
	if (_rpHandler != nullptr) {
		_rpHandler->updatePools(newPools, unchangedPools, disconnectedPools);
	}
}

void DrmInterface::offerResource(const set<Offer>& offers) {
	LOG(INFO) << "offer resource to pools";
	_rpoHandler->sendOffers(offers);
}

/* for thrift */
bool drm::Resource::operator<(drm::Resource const&) const { return false; };
bool drm::PoolWeight::operator<(drm::PoolWeight const&) const { return false; };
bool drm::Variable::operator<(drm::Variable const&) const { return false; }
bool drm::Offer::operator<(drm::Offer const&) const { return false; }
bool drm::PoolInfo::operator<(drm::PoolInfo const&) const { return false; }


