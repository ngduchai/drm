
#ifndef RESOURCE_PARTITIONER_H
#define RESOURCE_PARTITIONER_H

#include <thread>
#include <string>


#include <thrift/server/TSimpleServer.h>

#include "gen-cpp/ResourcePool.h"
#include "gen-cpp/ResourcePoolCb.h"

namespace drm {

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

class ResourcePool {
public:
	ResourcePool(std::string name);
	ResourcePool(std::string name, std::string drmHostname, int port);
	void initialize(std::string drmHostname, int drmPort);
	void finalize();
	
	void connect(Response& ret, const SecurityCredential& credential);
	void disconnect(Response& ret, const SecurityCredential& credential);
	void acceptOffer(Response& ret, const Offer& offer, const Resource& resources,
		const Operation& opt, const Filter& filter);
	void declineOffer(Response& ret, const Offer& offer, const Filter& filter);
	void releaseResource(Response& ret, const std::set<Resource>& resources);
	void launchTask(Response& ret, const String& agentId, const TaskInfo& task);


	// call back implemented by user
	virtual void processOffer(const std::set<Offer> & offers) = 0;
	virtual void processReclaimResource(const Resource& resources,
		const Integer timestamp, const Integer time) = 0;

protected:
	class Handler : public ResourcePoolCbIf { 
	public:
		Handler(ResourcePool *rpIf);
		virtual void processOffer(const std::set<Offer> & offers) {
			//std::thread t(&ResourcePool::processOffer, _rpIf, offers);
			_rpIf->processOffer(offers);
		}
		virtual void processReclaimResource(const Resource& resources,
				const Integer timestamp, const Integer time) {
			/*
			std::thread t(&ResourcePool::processReclaimResource, _rpIf, resources,
				timestamp, time);
			*/
			_rpIf->processReclaimResource(resources, timestamp, time);
		}
	private:
		ResourcePool *_rpIf = nullptr;
	};
	PoolInfo _info;
	bool _connected = false;
	ResourcePoolClient * _client = nullptr;
	std::thread * _cbthread = nullptr;
	TServer * _server = nullptr;
	void _startcb();
	virtual void initCallback(Response& _return, const SecurityCredential& credential);

};

}

#endif


