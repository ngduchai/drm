
#ifndef _DRM_INTERFACE_H_
#define _DRM_INTERFACE_H_

#include <thread>
#include <unordered_map>

#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/PlatformThreadFactory.h>
#include <thrift/processor/TMultiplexedProcessor.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/TToString.h>
#include <thrift/stdcxx.h>

#include "mesos/allocator/allocator.hpp"
#include "mesos/master/master.hpp"

#include "logging/flags.hpp"
#include "logging/logging.hpp"

#include "gen-cpp/message_constants.h"
#include "gen-cpp/message_types.h"
#include "gen-cpp/PerformanceStatistics.h"
#include "gen-cpp/ResourcePartitioner.h"
#include "gen-cpp/ResourcePartitionerCb.h"
#include "gen-cpp/ResourcePool.h"
#include "gen-cpp/ResourcePoolCb.h"
#include "gen-cpp/SystemAdmin.h"

namespace drm {

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
using namespace mesos::internal::master;

class ResourcePartitionerHandler : public ResourcePartitionerIf {
public:
  ResourcePartitionerHandler(Master *master) : _master(master) {};
  
  virtual void getWeight(std::set<PoolWeight> & _return);

  virtual void connect(Response& _return, const SecurityCredential& credential);
  virtual void disconnect(Response& _return, const SecurityCredential& credential);
  virtual void setWeight(Response& _return, const std::set<PoolWeight> & poolWeights);
  virtual void updateConnection(Response& _return, const SecurityCredential& credential,
		const String& hostname, Integer port);
  void updatePools(const std::set<PoolInfo> & newPools,
		const std::set<PoolInfo> & unchangedPools,
		const std::set<PoolInfo> & disconnectedPools);

 

  void zip() { LOG(INFO) << "zip()"; }
private:
  Master* _master;
  ResourcePartitionerCbClient *_cb = nullptr;
  bool validCredential(SecurityCredential& credential);
};

class SystemAdminHandler : public SystemAdminIf {
public:
	SystemAdminHandler(Master* master) : _master(master) {};
	
	virtual void addResource(Response& response, const std::set<Resource>& resources);
	virtual void removeResource(Response& response, const std::set<Resource>& resources,
		Integer timestamp, Integer time);
	virtual void restart(Response& response, Integer timestamp, Integer time);
	virtual void shutdown(Response& response, Integer timestamp, Integer time);

private:
	Master* _master;
};

class ResourcePoolHandler : public ResourcePoolIf {
public:
	ResourcePoolHandler(Master * master) : _master(master) {};
	
	virtual void connect(Response& ret, const PoolInfo& pool,
		const SecurityCredential& credential);
	virtual void disconnect(Response& ret, const String& poolId,
		const SecurityCredential& credential);
	virtual void acceptOffer(Response& ret, const Offer& offer, const Resource& resources,
		const Operation& opt, const Filter& filter);
	virtual void declineOffer(Response& ret, const Offer& offer, const Filter& filter);
	virtual void releaseResource(Response& ret, const std::set<Resource>& resources);
	virtual void launchTask(Response& ret, const String& agentId, const TaskInfo& task);
	
	virtual void updateConnection(Response& _return, const String& poolId,
		const SecurityCredential& credential, const String& hostname, Integer port);
	virtual void sendOffers(const std::set<Offer>& offers);
private:
	class PoolData {
	public:
		PoolInfo info;
		ResourcePoolCbClient *cb;
	};
	Master * _master;
	std::unordered_map<std::string, PoolData> _pools;
	bool validCredential(SecurityCredential& credential);
};

class PerformanceStatisticsHandler : public PerformanceStatisticsIf {
public:
	PerformanceStatisticsHandler(Master* master) : _master(master) {};
	
	virtual void retrievePoolAllocation(Resource& ret, const String& poolId);
	virtual void retrieveAllPoolsAllocation(std::set<Resource>& ret);

private:
	Master * _master;
	
};

/*
 *   ResourcePartitionerIfFactory is code generated.
 *   ResourcePartitionerCloneFactory is useful for getting access to the server side of the
 *   transport.  It is also useful for making per-connection state.  Without this
 *   CloneFactory, all connections will end up sharing the same handler instance.
 **/
/*
class ResourcePartitionerCloneFactory : virtual public ResourcePartitionerIfFactory {
 public:
  virtual ~ResourcePartitionerCloneFactory() {}
  virtual ResourcePartitionerIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo)
  {
    stdcxx::shared_ptr<TSocket> sock = stdcxx::dynamic_pointer_cast<TSocket>(connInfo.transport);
    LOG(INFO) << "Incoming connection";
    LOG(INFO) << "\tSocketInfo: "  << sock->getSocketInfo();
    LOG(INFO) << "\tPeerHost: "    << sock->getPeerHost();
    LOG(INFO) << "\tPeerAddress: " << sock->getPeerAddress();
    LOG(INFO) << "\tPeerPort: "    << sock->getPeerPort();
    return new ResourcePartitionerHandler;
  }
  virtual void releaseHandler(ResourcePartitionerIf* handler) {
    delete handler;
  }
};
*/

class DrmInterface {
public:
	DrmInterface(Master *master) : _master(master) {
		_master = master;
		initialize(12345);
	};
	
	void setMaster(Master *master) { _master = master; }
	void updatePools(const std::set<PoolInfo> & newPools,
		const std::set<PoolInfo> & unchangedPools,
		const std::set<PoolInfo> & disconnectedPools);
	void offerResource(const set<Offer>& offers);
	
private:
	std::thread * serverthread = nullptr;
	TServer *_server = nullptr;
	Master *_master = nullptr;
	
	stdcxx::shared_ptr<ResourcePartitionerHandler> _rpHandler;
	stdcxx::shared_ptr<SystemAdminHandler> _saHandler;
	stdcxx::shared_ptr<ResourcePoolHandler> _rpoHandler;
	stdcxx::shared_ptr<PerformanceStatisticsHandler> _psHandler;
	
	void initialize(int port);
	void finalize();
	void _startint();
	
};

}

#endif


