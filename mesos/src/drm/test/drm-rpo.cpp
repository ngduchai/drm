#include <iostream>

#include "../resource_pool.hpp"

#include <thread>
#include <chrono>
#include <unordered_map>

using namespace std;
using namespace drm;

class SimpleResourcePool : public ResourcePool {
public: 
	SimpleResourcePool(std::string name, std::string hostname, int port)
		: ResourcePool(name, hostname, port) {}
	void processOffer(const std::set<Offer> & offers) {
		std::cout << "Process offers:" << std::endl;
		std::cout << "Number of offers: " << offers.size() << std::endl;
		Filter filter;
		filter.refuse_seconds = 10;
		for (auto offer : offers) {
			offer.printTo(std::cout);
			std::cout << std::endl;
			
			Response ret;

			if (executors.find(offer.agent.id) == executors.end()) {
				executors[offer.agent.id] == true;
				// Use offer for starting an executor
				offer.resource.cpu.scalar = 1;
				offer.resource.mem.scalar = 512;
				offer.resource.disk.scalar = 0;
				
				// Create executor
				
				Operation opt;
				opt.type = drm::OperationType::LAUNCH;
				TaskInfo task;
				task.name = "executor";
				task.id = task.name + "-" + this->_info.id + std::to_string(scount);
				scount++;
				task.agentId = offer.agent.id;
				task.resources = offer.resource;
				task.cmd.cmd = "/home/ndhai/home/sources/drm/executor/test_executor";
				Variable var;
				var.name = "LD_LIBRARY_PATH";
				var.value = "/home/ndhai/local/lib";
				task.cmd.environment.insert(var);
				
				opt.__set_task(task);
				acceptOffer(ret, offer, offer.resource, opt, filter);
				
				std::cout << "Accept offer: ";
				opt.printTo(std::cout);
				std::cout << std::endl;
				
			}else{
				// Use offer for starting tasks

				declineOffer(ret, offer, filter);
				return;
				// Adjust resources
				offer.resource.cpu.scalar = 1;
				offer.resource.mem.scalar = 1024;
				offer.resource.disk.scalar = 0;
				
				// Create a task which will sleep for 10 seconds
				Operation opt;
				opt.type = drm::OperationType::LAUNCH;
				TaskInfo task;
				task.name = "sleep";
				task.id = task.name + "-" + this->_info.id + std::to_string(scount);
				scount++;
				task.agentId = offer.agent.id;
				task.resources = offer.resource;
				task.cmd.cmd = "sleep 10";
				
				opt.__set_task(task);
				acceptOffer(ret, offer, offer.resource, opt, filter);
			
			}
		}
	}
	virtual void processReclaimResource(const Resource& resources,
			const Integer timestamp, const Integer time) {
		std::cout << "process reclaim resource" << std::endl;
	}

private:
	std::unordered_map<std::string, bool> executors;
	unsigned long scount = 0;
};

int main() {
  SimpleResourcePool client("Test", "localhost", 12345);
  SecurityCredential credential;
  Response ret;
  client.connect(ret, credential);
  
  for (int i = 1; i < 10; ++i) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  }
  client.disconnect(ret, credential);

  return 0;
}


