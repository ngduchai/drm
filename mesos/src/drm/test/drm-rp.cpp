#include <iostream>

#include "../resource_partitioner.hpp"

#include <thread>
#include <chrono>

using namespace std;
using namespace drm;

class SimpleResourcePartitioner : public ResourcePartitioner {
public: 
	SimpleResourcePartitioner(std::string hostname, int port)
		: ResourcePartitioner(hostname, port) {}
	void onPoolChange(const std::set<PoolInfo> & newPools,
			const std::set<PoolInfo> & unchangedPools,
			const std::set<PoolInfo> & disconnectedPools) {
		cout << "Pools changed:" << endl;
		cout << "  "  << "New pools" << endl;
		for (auto pool : newPools) {
			cout << "    " << pool.name << " - " << pool.id << endl;
		}
		cout << "  "  << "Unchanged pools" << endl;
		for (auto pool : unchangedPools) {
			cout << "    " << pool.name << " - " << pool.id << endl;
		}
		cout << "  "  << "Disconnected pools" << endl;
		for (auto pool : disconnectedPools) {
			cout << "    " << pool.name << " - " << pool.id << endl;
		}
	}
};

int main() {
  SimpleResourcePartitioner client("localhost", 12345);
  SecurityCredential credential;
  Response ret;
  client.connect(ret, credential);
  try {
    std::set<PoolWeight> weights;
    std::set<PoolWeight> newWeights;
    client.getWeight(weights);
	cout << "Current weights:" << endl;
    for (auto weight : weights) {
      cout << "    " << weight.id << ": " << weight.weight << endl;
      weight.weight *= 2;
	  newWeights.insert(weight);
	}
	client.setWeight(ret, newWeights);
	if (ret.info.code != 0) {
	  cout << "Error occurs:, reason: " << ret.info.message << endl;
	}else{
	  cout << "New weight set:" << endl;
      client.getWeight(weights);
      for (auto weight : weights) {
        cout << "    " << weight.id << ": " << weight.weight << endl;
	  }
	}
  } catch (apache::thrift::transport::TTransportException& ex) {
    cout << "Request failed " << ex.what();
  }
  
  for (int i = 1; i < 10; ++i) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
  client.disconnect(ret, credential);
  cout << "Disconnected from server" << endl;

  return 0;
}


