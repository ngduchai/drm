
#ifndef DRM_POOL_H
#define DRM_POOL_H

class DrmPool : public ResourcePoolOwnerIf {
public:
	virtual void connect(Response& ret, const SecurityCredential& credential);
	virtual void disconnect(Response& ret, const SecurityCredential& credential);
	virtual void acceptOffer(Response& ret, const Offer& offer, const Resource& resources,
		const Operation& opt, const Filter& filter);
	virtual void declineOffer(Response& ret, const Offer& offer, const Filter& filter);
	virtual void releaseResource(Response& ret, const std::set<Resource>& resources);
	virtual void launchTask(Response& ret, const String& agentId, const TaskInfo& task);
	
	/* Callback functions */
	virtual void processOffer(const std::set<Offer> * offers) = 0;
	virtual void processReclaimResource(const Resource& resource, int timestamp, int time) = 0;
 
}

#endif


