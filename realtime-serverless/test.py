
import shlex

import subprocess
import os
from threading import Timer

def stop_cmd(proc):
	print("Time out")
	proc.kill()

def run(cmd, timeout_sec):
	proc = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	timer = Timer(timeout_sec, stop_cmd, args=(proc,))
	try:
		timer.start()
		stdout, stderr = proc.communicate()
		print("stdout - ", str(stdout))
		print("stderr - ", str(stderr))
	except Exception as e:
		print(e)
	finally:
		timer.cancel()

import time
print("Sleep")
print(int(round(time.time() * 1000)))
run("sleep 1", 2)
print(int(round(time.time() * 1000)))
print("Sleep")
run("cat test.py", 1)




