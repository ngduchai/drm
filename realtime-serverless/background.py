
import shlex

import subprocess
import os
from threading import Timer

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import numpy as np

from threading import Thread, Lock, Condition

import pylab as pl
import os
import sys

import time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

#data_path = 'data/material'
data_path = 'frames'

hostname = sys.argv[1]
port = sys.argv[2]

frame_check = Condition()
buf = []

times = []

def create_frames(name, client, num):
	time.sleep(max(1, np.sum(np.random.normal(2, 2, 10)) / 10))
	
	transport = TSocket.TSocket(hostname, port)
	transport = TTransport.TBufferedTransport(transport)
	protocol = TBinaryProtocol.TBinaryProtocol(transport)
	client = RealtimeServerless.Client(protocol)
	
	transport.open()
	
	client.add_user('test')
	
	print("Open connection")
	
	for i in np.random.uniform(0, 70, num):
		fpath = '/home/ndhai/home/sources/drm/realtime-serverless/frames/'
		#tpath = 'ndhai@linux.cs.uchicago.edu:/home/ndhai/realtime-serverless/data/material/'
		tpath = '/home/ndhai/home/sources/drm/realtime-serverless/data/material/'
		frame = 'frame_' + str(i).zfill(2) + '_delay-0.1s.png'
		#cmd = 'scp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		#cmd = 'cp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		height = 48
		while (True):
			height = np.sum(np.random.normal(48, 23, 30)) / 30
			if (height >= 20 and height <= 200):
				break
		height = height / 5
		cmd = 'sleep ' + str(height)
		#print('Run:', cmd)
		while (True):
			start = time.time()
			iid = client.execute(name, cmd)
			end = time.time()
			runtime = end - start
			if (runtime < 1):
				time.sleep(1 - runtime)
			if (iid != ''):
				times.append(time.time())
				frame_check.acquire()
				buf.append(i)
				frame_check.notify()
				frame_check.release()
				break
		print(time.time(), i)

	transport.open()

"""
# Connect to Mesos framework
transport = TSocket.TSocket(hostname, port)
transport = TTransport.TBufferedTransport(transport)
protocol = TBinaryProtocol.TBinaryProtocol(transport)
client = RealtimeServerless.Client(protocol)
	
transport.open()

print("Connected to framework, start to send frames")

client.add_user('test')
"""

num = 1000
users = []
#users = ['test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test']
users = ['test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test',
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test',
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test']

"""
users = ['test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test',
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test'
		'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test']
"""

for user in users:
	#thread = Thread(target=create_frames, args=(user, client, num,))
	thread = Thread(target=create_frames, args=(user, None, num,))
	thread.setDaemon(True)
	thread.start()

while (True):
	time.sleep(100)
	



transport.close()


