
namespace py rtsl

typedef i32 int

struct ExecutorStatus {
	1: int cap,
	2: int free,
	3: string hostname,
	4: int port
}

struct TaskStatus {
	1: string code,
	2: string stdout,
	3: string stderr
}

struct Info {
	1: int cap,
	2: int free,
	3: int min_cap
}

service RealtimeServerless {
	int add_user(1: string name)
	string execute(1: string user, 2: string cmd)
	Info info()
	TaskStatus taskInfo(1: string id)
}

service ExecutorInt {
	string run(1: string id, 2: string cmd)
	ExecutorStatus get_status()
	int terminate(1: int num)
}

service SchedulerInt {
	int finish(1: string eid, 2: string iid, 3: TaskStatus stat)
	int update(1: string id, 2: ExecutorStatus stat)
	int clean(1: string eid, 2: int num)
}


