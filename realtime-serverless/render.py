
import shlex

import subprocess
import os
from threading import Timer

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import pylab as pl
import os

import time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

data_path = 'data/material'

"""
# Clean frames
for f in os.listdir(data_path):
    try:
        path = data_path + '/' + f
        if os.path.isfile(path):
            os.unlink(path)
    except Exception as e:
        print("ERROR:", e)

print("Show figure")
"""

# Show figures
index = 0
path = data_path + '/' + str(index) + '.png'
img = None
while (True):
    if os.path.exists(path):
        im = pl.imread(path)
        index += 1
        path = data_path + '/' + str(index) + '.png'
        if img is None:
            img = pl.imshow(im)
        else:
            img.set_data(im)
        pl.pause(0.1)
        pl.draw()
    time.sleep(0.1)


transport.close()


