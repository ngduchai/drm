
import shlex

import subprocess
import os
from threading import Timer

import matplotlib
#matplotlib.use('GTKAgg')
matplotlib.use('tkAgg')

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.animation as animation
import matplotlib.image as mpimg

import numpy as np

import pickle

from threading import Thread, Lock, Condition

import pylab as pl
import os
import sys
import signal

import time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

import common

#data_path = 'data/material'
data_path = 'frames'

#hostname = sys.argv[1]
#port = sys.argv[2]

rtsl_frame_check = Condition()
rtsl_buf = []

sl_frame_check = Condition()
sl_buf = []


ttime = time.time()

ideal_times = []
ideal_trates = []
ideal_rates = []

rtsl_times = []
rtsl_trates = []
rtsl_rates = []
rtsl_res = []

sl_times = []
sl_trates = []
sl_rates = []
sl_res = []

FRAME_PRE = '/frame_' 
FRAME_SUF = '_delay-0.1s.png'
NUM_FRAMES = 70

grid = plt.GridSpec(2, 3)
fig = plt.figure()
ideal_frames_fig = plt.subplot(grid[0, 0])
rtsl_frames_fig = plt.subplot(grid[0, 1])
sl_frames_fig = plt.subplot(grid[0, 2])
res_fig = plt.subplot(grid[1, 0])
rate_fig = plt.subplot(grid[1, 1])
latency_fig = plt.subplot(grid[1, 2])

ideal_index = 0
rtsl_allocs = 0
sl_allocs = 0

tmutex = Lock()
rtsl_tasks = []
sl_tasks = []
keep_check_res = True

def show_figures():

	def animate(i):
		global calloc
		
		global rtsl_allocs
		global rtsl_times
		global rtsl_rates
		
		global sl_allocs
		global sl_times
		global sl_rates

		global ideal_index
		global ideal_times
		
		start = time.time()
		
		# Draw latency
		if ideal_index < NUM_FRAMES - 1:
			ideal_times.append(ideal_index)
		latency_fig.clear()
		latency_fig.plot(range(0, len(ideal_times)), ideal_times, label='Ideal', color='orange')
		latency_fig.plot(range(0, len(rtsl_times)), rtsl_times, label='RS', color='blue')
		latency_fig.plot(range(0, len(sl_times)), sl_times, label='serverless', color='green')
		latency_fig.set_xlabel('Frames')
		latency_fig.set_ylabel('Latency (second)')
		latency_fig.legend(loc = 'best')
		
		# Draw allocation rate
		ctime = time.time() - ttime
		print(ctime)
		while (rtsl_allocs <= len(rtsl_times) - 1):
			if (ctime - rtsl_times[rtsl_allocs] > 5):
				rtsl_allocs += 1
			else:
				break
		rtsl_trates.append(ctime)
		rtsl_rates.append(float(len(rtsl_times) - rtsl_allocs) / 5)
		
		while (sl_allocs <= len(sl_times) - 1):
			if (ctime - sl_times[sl_allocs] > 5):
				sl_allocs += 1
			else:
				break
		sl_trates.append(ctime)
		sl_rates.append(float(len(sl_times) - sl_allocs) / 5)
		
		ideal_trates.append(ctime)
		ideal_rates.append(5 / 5)
	
		
		rate_fig.clear()
		rate_fig.plot(ideal_trates, ideal_rates, label='Ideal', color='orange')
		rate_fig.plot(rtsl_trates, rtsl_rates, label='RS', color='blue')
		rate_fig.plot(sl_trates, sl_rates, label='serverless', color='green')
		rate_fig.set_ylim(ymin=0)
		rate_fig.set_xlabel('Time (second)')
		rate_fig.set_ylabel('Alloc. rate')
		rate_fig.legend(loc = 'best')
		

		# Draw resource utilization
		rtsl_res.append(len(rtsl_tasks))
		sl_res.append(len(sl_tasks))
		res_fig.clear()
		res_fig.plot(range(0, len(rtsl_trates)), rtsl_res, label='RS', color='blue')
		res_fig.plot(range(0, len(sl_trates)), sl_res, label='serverless', color='green')
		res_fig.set_xlabel('Time')
		res_fig.set_ylabel('Resource (instance)')
		res_fig.legend(loc = 'best')

		
		index = 0
		rtsl_frame_check.acquire()
		try:
			if (len(rtsl_buf) > 0):
				index = rtsl_buf.pop(0)
				path = data_path + FRAME_PRE + str(index).zfill(2) + FRAME_SUF
				rtsl_frames_fig.imshow(mpimg.imread(path))
				rtsl_frames_fig.set_title('STREAMING USING RS')
		finally:
			rtsl_frame_check.release()
		
		index = 0
		sl_frame_check.acquire()
		try:
			if (len(sl_buf) > 0):
				index = sl_buf.pop(0)
				path = data_path + FRAME_PRE + str(index).zfill(2) + FRAME_SUF
				sl_frames_fig.imshow(mpimg.imread(path))
				sl_frames_fig.set_title('STREAMING USING SERVERLESS')
		finally:
			sl_frame_check.release()
		
		path = data_path + FRAME_PRE + str(ideal_index).zfill(2) + FRAME_SUF
		ideal_frames_fig.imshow(mpimg.imread(path))
		ideal_frames_fig.set_title('IDEAL')
		if ideal_index < NUM_FRAMES - 1:
			ideal_index += 1
		
		"""
		end = time.time()
		runtime = end - start
		#print("runtime:", runtime)
		if (runtime < 0.7):
			time.sleep(0.7 - runtime)
			#print("total runtime:", time.time() - start)
		"""
		
	#ani = animation.FuncAnimation(fig, animate, interval=1000)
	ani = animation.FuncAnimation(fig, animate, interval=800)
	plt.show()
	



def create_frames(name, client, frame_check, buf, times, tasks):
	for i in range(0, 70):
		fpath = '/home/ndhai/home/sources/drm/realtime-serverless/frames/'
		#tpath = 'ndhai@linux.cs.uchicago.edu:/home/ndhai/realtime-serverless/data/material/'
		tpath = '/home/ndhai/home/sources/drm/realtime-serverless/data/material/'
		frame = FRAME_PRE + str(i).zfill(2) + FRAME_SUF
		#cmd = 'scp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		#cmd = 'cp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		height = 48
		while (True):
			height = np.sum(np.random.normal(48, 23, 30)) / 30
			if (height >= 20 and height <= 200):
				break
		height = height / 5
		cmd = 'sleep ' + str(height)
		#cmd = 'sleep 2'
		#print('Run:', cmd)
		while (True):
			start = time.time()
			iid = client.execute(name, cmd)
			end = time.time()
			runtime = end - start
			if (runtime < 1):
				time.sleep(1 - runtime)
			if (iid != ''):
				#print(time.time())
				times.append(time.time() - ttime)
				frame_check.acquire()
				buf.append(i)
				frame_check.notify()
				frame_check.release()
				
				tmutex.acquire()
				tasks.append(iid)
				tmutex.release()
				break
	#print(times)

def check_resource(rtsl_client, sl_client):
	global rtsl_tasks
	global sl_tasks
	while keep_check_res:
		tmutex.acquire()
		if (len(rtsl_tasks) > 0):
			for task in rtsl_tasks[:]:
				stat = rtsl_client.taskInfo(task)
				if stat.code != common.TASK_RUNNING and stat.code != common.TASK_STARTED:
					rtsl_tasks.remove(task)
		tmutex.release()
		
		tmutex.acquire()
		if (len(sl_tasks) > 0):
			stats = []
			for task in sl_tasks[:]:
				stat = sl_client.taskInfo(task)
				if stat.code != common.TASK_RUNNING and stat.code != common.TASK_STARTED:
					sl_tasks.remove(task)
				stats.append(stat.code)
			print(stats)
		tmutex.release()
		
		time.sleep(0.5)

def print_graphs(rtsl_thread, sl_thread):
	rtsl_thread.join()
	sl_thread.join()
	print("ideal_times", ideal_times)
	print("rtsl_times", rtsl_times)
	print("sl_times", sl_times)
	print("rtsl_rates", rtsl_rates)
	print("sl_rates", sl_rates)
	with open('monitor-1.data', 'wb') as f:
		pickle.dump(ideal_times, f)
		pickle.dump(rtsl_times, f)
		pickle.dump(sl_times, f)
		pickle.dump(ideal_rates, f)
		pickle.dump(rtsl_rates, f)
		pickle.dump(sl_rates, f)
		pickle.dump(ideal_trates, f)
		pickle.dump(rtsl_trates, f)
		pickle.dump(sl_trates, f)
		pickle.dump(rtsl_res, f)
		pickle.dump(sl_res, f)




# Connect to Mesos framework
hostname = 'river-fe4'
port = 12345
rtsl_transport = TSocket.TSocket(hostname, port)
rtsl_transport = TTransport.TBufferedTransport(rtsl_transport)
protocol = TBinaryProtocol.TBinaryProtocol(rtsl_transport)
rtsl_client = RealtimeServerless.Client(protocol)
rtsl_transport.open()
rtsl_client.add_user('test')

hostname = 'river-fe2'
port = 12346
sl_transport = TSocket.TSocket(hostname, port)
sl_transport = TTransport.TBufferedTransport(sl_transport)
protocol = TBinaryProtocol.TBinaryProtocol(sl_transport)
sl_client = RealtimeServerless.Client(protocol)
sl_transport.open()
sl_client.add_user('test')



print("Connected to frameworks, start to send frames")


rtsl_thread = Thread(target=create_frames,
		args=('test', rtsl_client, rtsl_frame_check, rtsl_buf, rtsl_times, rtsl_tasks))
rtsl_thread.start()
sl_thread = Thread(target=create_frames,
		args=('test', sl_client, sl_frame_check, sl_buf, sl_times, sl_tasks))
sl_thread.start()
thread = Thread(target=print_graphs, args=(rtsl_thread, sl_thread))
thread.start()

print("Create background load")

cmd = 'python background.py river-fe2 12346'
rtsl_proc = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
cmd = 'python background.py river-fe4 12346'
sl_proc = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def signal_handler(signal, frame):
	rtsl_proc.kill()
	sl_proc.kill()
	keep_check_res = False
	print("Terminate backgound load")

signal.signal(signal.SIGINT, signal_handler)

thread = Thread(target=check_resource, args=(rtsl_client, sl_client))
thread.start()

print("Show figure")

ttime = time.time()
show_figures()

"""
# Show figures
index = 0
img = None
while (True):
	frame_check.acquire()
	while (len(buf) == 0):
		frame_check.wait(1)
	index = buf.pop(0)
	frame_check.release()
	path = data_path + '/frame_' + str(index).zfill(2) + '_delay-0.1s.png'
	im = pl.imread(path)
	if img is None:
		img = pl.imshow(im)
	else:
		img.set_data(im)
	pl.pause(0.1)
	pl.draw()
	#time.sleep(0.1)
"""

rtsl_transport.close()
sl_transport.close()

rtsl_proc.kill()
sl_proc.kill()

keep_check_res = False

with open('monitor.data', 'wb') as f:
	pickle.dump(ideal_times, f)
	pickle.dump(rtsl_times, f)
	pickle.dump(sl_times, f)
	pickle.dump(ideal_rates, f)
	pickle.dump(rtsl_rates, f)
	pickle.dump(sl_rates, f)
	pickle.dump(ideal_trates, f)
	pickle.dump(rtsl_trates, f)
	pickle.dump(sl_trates, f)
	pickle.dump(rtsl_res, f)
	pickle.dump(sl_res, f)


print("Clean execution")

