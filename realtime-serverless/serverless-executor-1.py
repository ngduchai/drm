#!/usr/bin/env python2.7
from __future__ import print_function

import sys
import time
from threading import Thread, Lock, Condition, Timer

#from threading import Timer

import socket

from pymesos import MesosExecutorDriver, Executor, decode_data
from addict import Dict

import common

from multiprocess import Process

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import ttypes, ExecutorInt, SchedulerInt

import shlex

import subprocess
import os

class ScStatus:
	def __init__(self, hostname, port):
		self.hostname = hostname
		self.port = port
		self.conn = None
		self.trans = None

class Task:
	def __init__(self, iid, cmd):
		self.iid = iid
		self.cmd = cmd
		self.stat = common.TASK_STARTED

class ExecutorIntHandler:
	def __init__(self, executor):
		self.log = {}
		self.executor = executor
	
	def run(self, iid, cmd):
		task = Task(iid, cmd)
		self.executor.hastask.acquire()
		try:
			self.executor.wtasks.append(task)
			self.executor.hastask.notify()
		except Exception as e:
			print("Error:", e)
			return common.TASK_KILLED
		finally:
			self.executor.hastask.release()
		return common.TASK_STARTED
		
	
	def get_status(self):
		self.executor.smutex.acquire()
		try:
			stat = ttypes.ExStatus()
			stat.cap = self.executor.cap
			stat.free = self.executor.free
			stat.hostname = self.executor.hostname
			stat.port = self.executor.port
		except Exception as e:
			print(e)
		finally:
			self.executor.smutex.release()
		return stat
	
	def terminate(self, num):
		print("Start terminate", num)
		self.executor.smutex.acquire()
		print("Run terminate")
		try:
			num = min(num, self.executor.cap)
			self.executor.ftasks += num
		except Exception as e:
			print("ERROR: " + str(e))
		finally:
			self.executor.smutex.release()
		print("Exe terminate")
		return num


class RealtimeServerlessExecutor(Executor):
	
	def __init__(self, port):
		super(RealtimeServerlessExecutor, self).__init__()
		self.eid = None
		self.hostname = socket.gethostname()
		self.port = port
		self.wtasks = []
		self.rtasks = {}
		self.smutex = Lock()
		self.hastask = Condition()
		self.sched = None
		self.cap = 0
		self.free = 0
		self.ftasks = 0
		
		self.listen(port)
	
	def launchTask(self, driver, task):
		def run_task(task):
			update = Dict()
			update.task_id.value = task.task_id.value
			update.state = 'TASK_RUNNING'
			update.timestamp = time.time()
			update.message = common.TASK_STARTED
			driver.sendStatusUpdate(update)
			
			#print(decode_data(task.data), file=sys.stderr)
			print(task.iid, task.cmd, file=sys.stderr)
			time.sleep(20)
		
		def stop_task(proc, stat):
			print("Kill task because of time out")
			proc.kill()
			stat.code = common.TASK_KILLED
			
		def start_instance(driver, task_info):
			complete = False
			while (not complete):
				task = None
				self.hastask.acquire()
				while (len(self.wtasks) == 0):
					self.hastask.wait()
				task = self.wtasks.pop(0)
				self.hastask.release()
				
				self.smutex.acquire()
				try:
					self.rtasks[task.iid] = task
					self.free -= 1
				finally:
					self.smutex.release()
				
				stat = ttypes.TaskStatus()
				stat.code = common.TASK_FINISHED
				print("Run task -- ", task.cmd)
				proc = subprocess.Popen(shlex.split(task.cmd),
						stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				timer = Timer(common.TASK_DURATION, stop_task, args=(proc, stat))
				try:
					timer.start()
					stdout, stderr = proc.communicate()
				except Exception as e:
					print(e)
				finally:
					timer.cancel()
					stat.stdout = str(stdout)
					stat.stderr = str(stderr)
				self.sched.conn.finish(self.eid, task.iid, stat)
				
				self.smutex.acquire()
				try:
					del self.rtasks[task.iid]
					self.free += 1
				finally:
					self.smutex.release()
				
				
				# Now check if executor want to reduce the number of instances
				self.smutex.acquire()
				try:
					if (self.ftasks > 0):
						complete = True
						self.ftasks -= 1
						self.cap -= 1
						self.free -= 1
				except Exception as e:
					print("ERROR: ", str(e))
				finally:
					self.smutex.release()
				
			
				"""
				proc = Process(target=run_task, args=(task,))
				proc.start()
				proc.join(common.TASK_DURATION)
				
				if proc.is_alive():
					proc.terminate()
					print("Task", task.iid, "reaches runtime limit, killed")
					self.sched.conn.finish(self.eid, task.iid, common.TASK_KILLED)
				else:
					self.sched.conn.finish(self.eid, task.iid, common.TASK_FINISHED)
				del self.rtasks[task.iid]
				"""
			
			# Let Mesos know the instance finished
			update = Dict()
			update.task_id.value = task_info.task_id.value
			update.state = 'TASK_FINISHED'
			update.timestamp = time.time()
			driver.sendStatusUpdate(update)
			
			# Let the scheduler know
			self.sched.conn.clean(self.eid, 1)
			
			
			print("Instance ", task_info.task_id.value, " finished")
			return 0
		
		"""
		update = Dict()
		update.task_id.value = task.task_id.value
		update.state = 'TASK_RUNNING'
		update.timestamp = time.time()
		update.message = common.TASK_STARTED
		driver.sendStatusUpdate(update)
		"""
		
		if (self.sched is None):
			print("Connect to scheduler")
			self.smutex.acquire()
			try:
				if (self.sched is None):
					# Creat connection
					connection = decode_data(task.data).split(':')
					self.eid = connection[0]
					hostname = connection[1]
					port = connection[2]
					self.sched = ScStatus(hostname, port)
					transport = TSocket.TSocket(hostname, port)
					transport = TTransport.TBufferedTransport(transport)
					protocol = TBinaryProtocol.TBinaryProtocol(transport)
					self.sched.conn = SchedulerInt.Client(protocol)
					transport.open()
					self.sched.trans = transport
			except Exception as e:
				print(e)
			finally:
				self.smutex.release()
		
		# Run new instance
		thread = Thread(target=start_instance, args=(driver,task,))
		thread.setDaemon(True)
		thread.start()
		
		# Update status
		stat = ttypes.ExecutorStatus()
		print("Update start")
		self.smutex.acquire()
		print("Update exe")
		try:
			self.cap += 1
			self.free += 1
			stat.hostname = self.hostname
			stat.port = self.port
			stat.cap = self.cap
			stat.free = self.free
		except Exception as e:
			print(e)
		finally:
			self.smutex.release()
		return self.sched.conn.update(self.eid, stat)
				
	def close():
		if (self.sched.trans is not None):
			self.sched.trans.close()

	def listen(self, port):
		
		handler = ExecutorIntHandler(self)
		processor = ExecutorInt.Processor(handler)
		transport = TSocket.TServerSocket(port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		#executor = TServer.TThreadedServer(processor, transport, tFactory, pFactory)
		executor = TServer.TThreadPoolServer(processor, transport, tFactory, pFactory)
		
		print("Starting Executor...")
		
		def runexecutor(executor):
			executor.serve()

		thread = Thread(target=runexecutor, args=(executor,))
		thread.setDaemon(True)
		thread.start()
		print("Executor is ready")


if __name__ == '__main__':
	import logging
	logging.basicConfig(level=logging.DEBUG)
	
	#driver = MesosExecutorDriver(RealtimeServerlessExecutor(), use_addict=True)
	executor = RealtimeServerlessExecutor(12301)
	driver = MesosExecutorDriver(executor, use_addict=True)

	
	driver.run()
