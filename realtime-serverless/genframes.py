
import shlex

import subprocess
import os
from threading import Timer

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import pylab as pl
import os

import time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

data_path = 'data/material'


    

# Connect to Mesos framework
transport = TSocket.TSocket('river-fe.cs.uchicago.edu', 12345)
transport = TTransport.TBufferedTransport(transport)
protocol = TBinaryProtocol.TBinaryProtocol(transport)
client = RealtimeServerless.Client(protocol)
	
transport.open()

print("Connected to framework, start to send frames")

client.add_user('test')

time.sleep(2)

for i in range(0, 70):
    fpath = '/home/ndhai/home/sources/drm/realtime-serverless/frames/'
    #tpath = 'ndhai@linux.cs.uchicago.edu:/home/ndhai/realtime-serverless/data/material/'
    tpath = '/home/ndhai/home/sources/drm/realtime-serverless/data/material/'
    frame = 'frame_' + str(i).zfill(2) + '_delay-0.1s.png'
    #cmd = 'scp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
    cmd = 'cp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
    print('Run:', cmd)
    while (True):
        iid = client.execute('test', cmd)
        time.sleep(1)
        if (iid != ''):
            break



transport.close()


