
import shlex

import subprocess
import os
from threading import Timer

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from threading import Thread, Lock

import pylab as pl
import os
import sys

import time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

data_path = 'data/material'

hostname = sys.argv[1]
port = sys.argv[2]

def create_frames(name, client):
	for i in range(0, 70):
		fpath = '/home/ndhai/home/sources/drm/realtime-serverless/frames/'
		#tpath = 'ndhai@linux.cs.uchicago.edu:/home/ndhai/realtime-serverless/data/material/'
		tpath = '/home/ndhai/home/sources/drm/realtime-serverless/data/material/'
		frame = 'frame_' + str(i).zfill(2) + '_delay-0.1s.png'
		#cmd = 'scp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		cmd = 'cp ' + fpath + frame + ' ' + tpath + str(i) + '.png'
		#cmd = 'sleep 7'
		print('Run:', cmd)
		while (True):
			start = time.time()
			iid = client.execute(name, cmd)
			end = time.time()
			runtime = end - start
			if (runtime < 1):
				time.sleep(1 - runtime)
			if (iid != ''):
				break


# Clean frames
for f in os.listdir(data_path):
    try:
        path = data_path + '/' + f
        if os.path.isfile(path):
            os.unlink(path)
    except Exception as e:
        print("ERROR:", e)

print("Show figure")

# Connect to Mesos framework
transport = TSocket.TSocket(hostname, port)
transport = TTransport.TBufferedTransport(transport)
protocol = TBinaryProtocol.TBinaryProtocol(transport)
client = RealtimeServerless.Client(protocol)
	
transport.open()

print("Connected to framework, start to send frames")

client.add_user('test')

thread = Thread(target=create_frames, args=('test', client,))
thread.setDaemon(True)
thread.start()


# Show figures
index = 0
path = data_path + '/' + str(index) + '.png'
img = None
while (True):
    if os.path.exists(path):
        im = pl.imread(path)
        index += 1
        path = data_path + '/' + str(index) + '.png'
        if img is None:
            img = pl.imshow(im)
        else:
            img.set_data(im)
        pl.pause(0.1)
        pl.draw()
    time.sleep(0.1)


transport.close()


