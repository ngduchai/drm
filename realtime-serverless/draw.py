
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.animation as animation
import matplotlib.image as mpimg

import pickle

import numpy as np

ideal_times = []
rtsl_times = []
sl_times = []
ideal_rates = []
rtsl_rates = []
sl_rates = []
ideal_trates = []
rtsl_trates = []
sl_trates = []
rtsl_res = []
sl_res = []


with open('monitor.data', 'rb') as f:
	ideal_times = pickle.load(f)
	rtsl_times = pickle.load(f)
	sl_times = pickle.load(f)
	ideal_rates = pickle.load(f)
	rtsl_rates = pickle.load(f)
	sl_rates = pickle.load(f)
	ideal_trates = pickle.load(f)
	rtsl_trates = pickle.load(f)
	sl_trates = pickle.load(f)
	rtsl_res = pickle.load(f)
	sl_res = pickle.load(f)


text_size = 14

plt.rc('xtick', labelsize=text_size)
plt.rc('ytick', labelsize=text_size)
plt.rc('axes', labelsize=text_size)
plt.rc('legend', fontsize=text_size)

# Draw resouce utilization
plt.figure()
plt.plot(range(0, len(rtsl_trates)), rtsl_res, label='RS', color='blue')
plt.plot(range(0, len(sl_trates)), sl_res, label='serverless', color='green')
plt.xlabel('Time')
plt.ylabel('Resource (instance)')
plt.legend(loc = 'best')

plt.tight_layout()

plt.figure()
#ideal_rates = np.ones(len(rtsl_rates))
#ideal_trates = rtsl_trates
plt.plot(ideal_trates, ideal_rates, label='Ideal', color='orange')
plt.plot(rtsl_trates, rtsl_rates, label='RS', color='blue')
plt.plot(sl_trates, sl_rates, label='serverless', color='green')
plt.ylim(ymin=0)
plt.xlabel('Time (second)')
plt.ylabel('Alloc. rate')
plt.legend(loc = 'best')
		
plt.tight_layout()

plt.figure()
plt.plot(range(0, len(ideal_times)), ideal_times, label='Ideal', color='orange')
plt.plot(range(0, len(rtsl_times)), rtsl_times, label='RS', color='blue')
plt.plot(range(0, len(sl_times)), sl_times, label='serverless', color='green')
plt.xlabel('Frames')
plt.ylabel('Latency (second)')
plt.legend(loc = 'best')
		
plt.tight_layout()

plt.show()


