#!/usr/bin/env python2.7
from __future__ import print_function

import sys
import time
from threading import Thread, Lock, Condition, Timer

#from threading import Timer

import socket

from pymesos import MesosExecutorDriver, Executor, decode_data
from addict import Dict

import common

from multiprocess import Process

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import ttypes, ExecutorInt, SchedulerInt

import shlex

import subprocess
import os

class ScStatus:
	def __init__(self, hostname, port):
		self.hostname = hostname
		self.port = port
		self.conn = None
		self.trans = None

class Task:
	def __init__(self, iid, cmd):
		self.iid = iid
		self.cmd = cmd
		self.stat = common.TASK_STARTED

class ExecutorIntHandler:
	def __init__(self, executor):
		self.log = {}
		self.executor = executor
	
	def run(self, iid, cmd):
		task = Task(iid, cmd)
		self.executor.hastask.acquire()
		try:
			self.executor.wtasks.append(task)
			self.executor.hastask.notify()
		except Exception as e:
			print(e)
			return 1
		finally:
			self.executor.hastask.release()
		return common.TASK_STARTED
		
	
	def get_status(self):
		self.executor.smutex.acquire()
		try:
			stat = ttypes.ExStatus()
			stat.cap = self.executor.cap
			stat.free = self.executor.free
			stat.hostname = self.executor.hostname
			stat.port = self.executor.port
		except Exception as e:
			print(e)
		finally:
			self.executor.smutex.release()
		return stat
	
	def terminate(self, num):
		self.executor.smutex.acquire()
		try:
			num = min(num, self.executor.cap)
			self.executor.ftasks += num
		except Exception as e:
			print("ERROR: " + str(e))
		finally:
			self.executor.smutex.release()
		return num


class RealtimeServerlessExecutor(Executor):
	
	def __init__(self, port):
		super(RealtimeServerlessExecutor, self).__init__()
		self.eid = None
		self.hostname = socket.gethostname()
		self.port = port
		self.wtasks = []
		self.rtasks = {}
		self.smutex = Lock()
		self.hastask = Condition()
		self.sched = None
		self.cap = 0
		self.free = 0
		self.ftasks = 0
		
		self.listen(port)
	
	def launchTask(self, driver, task):
		def stop_task(proc, stat):
			print("Kill task because of time out")
			proc.kill()
			stat.code = common.TASK_KILLED
		
		cmd = decode_data(task.data)
		iid = task.task_id.value
		print("Run task -- ", cmd)
		
		stat = ttypes.TaskStatus()
		stat.code = common.TASK_FINISHED
		proc = subprocess.Popen(shlex.split(cmd),
				stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		timer = Timer(common.TASK_DURATION, stop_task, args=(proc, stat))
		try:
			timer.start()
			stdout, stderr = proc.communicate()
		except Exception as e:
			print(e)
		finally:
			timer.cancel()
			stat.stdout = str(stdout)
			stat.stderr = str(stderr)
		#self.sched.conn.finish(self.eid, iid, stat)
		
		# Let Mesos know the instance finished
		update = Dict()
		update.task_id.value = task.task_id.value
		update.state = 'TASK_FINISHED'
		update.timestamp = time.time()
		driver.sendStatusUpdate(update)
			
		print("Instance ", task.task_id.value, " finished")
		return 0
		
				
	def close():
		if (self.sched.trans is not None):
			self.sched.trans.close()

	def listen(self, port):
		
		handler = ExecutorIntHandler(self)
		processor = ExecutorInt.Processor(handler)
		transport = TSocket.TServerSocket(port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		executor = TServer.TSimpleServer(processor, transport, tFactory, pFactory)
		
		print("Starting Executor...")
		
		def runexecutor(executor):
			executor.serve()

		thread = Thread(target=runexecutor, args=(executor,))
		thread.setDaemon(True)
		thread.start()
		print("Executor is ready")


if __name__ == '__main__':
	import logging
	logging.basicConfig(level=logging.DEBUG)
	
	#driver = MesosExecutorDriver(RealtimeServerlessExecutor(), use_addict=True)
	executor = RealtimeServerlessExecutor(12301)
	driver = MesosExecutorDriver(executor, use_addict=True)

	
	driver.run()
