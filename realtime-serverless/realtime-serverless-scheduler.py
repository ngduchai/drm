#!/usr/bin/env python2.7
from __future__ import print_function

import sys
import uuid
import time
import socket
import signal
import getpass
from threading import Thread, Lock, Condition
from os.path import abspath, join, dirname

from pymesos import MesosSchedulerDriver, Scheduler, encode_data
from addict import Dict

import datetime

#TASK_CPU = 0.1
#TASK_MEM = 32
#EXECUTOR_CPUS = 0.1
#EXECUTOR_MEM = 32

import common

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless, SchedulerInt, ExecutorInt, ttypes

import random
import uuid
import time

class TaStatus:
	def __init__(self, iid):
		self.iid = iid
		self.stdout = ''
		self.stderr = ''
		self.start = 0
		self.end = 0
		self.code = ''


class ExStatus:

	def __init__(self, eid):
		self.reset()
		self.eid = eid

	def __init__(self, eid, cap, free, hostname, port):
		self.reset()
		self.eid = eid
		self.cap = cap
		self.free = free
		self.hostname = hostname
		self.port = port
	
	def __init__(self, eid, stat):
		self.reset()
		self.eid = eid
		self.cap = stat.cap
		self.free = stat.free
		self.hostname = stat.hostname
		self.port = stat.port
	
	def reset(self):
		self.eid = ''
		self.cap = 0
		self.free = 0
		self.hostname = ""
		self.port = 0
		self.conn = None
		self.trans = None
	
	def close():
		if self.trans is not None:
			trans.close()


schedalloc = Condition()

class RealtimeServerlessIntHandler:
	def __init__(self, scheduler):
		self.log = {}
		self.scheduler = scheduler
		self.hasinstance = Condition
		self.executions = {}
	
	def execute(self, user, cmd):
		if (user not in self.executions):
			return ''
		schedalloc.acquire()
		while (len(self.scheduler.free_executors) == 0):
			if (self.scheduler.cap == self.scheduler.minCap):
				# If stress load happen, we are in alarmming situation,
				# increase resource allocation to make sure we can ensure the guaranteed
				# allocation rate
				self.scheduler.minCap += common.POOL_CAP
			#schedalloc.wait(1/common.ALLOC_RATE)
			schedalloc.release()
			print('WARNING Pool is full')
			return ''
		
		# Randomly choose an executor with at least 1 free instance, process task on it
		self.scheduler.smutex.acquire()
		
		iid = ''
		try:
			# Now check for resource utilization again, make sure we always have a large enough
			# buffer for burst interference
			if (self.scheduler.free < common.BUFFER_SIZE and
					self.scheduler.cap == self.scheduler.minCap):
				self.scheduler.minCap += common.BUFFER_SIZE
			
			# Start new instance for the comming task
			# but we have to adjust the allocation rate to avoid bursts
			while (True):
				current = int(round(time.time() * 1000))
				executions = self.executions[user]
				while (len(executions) > 0 and current - executions[0] > 1000):
					executions.pop(0)
				if (len(executions) == common.ALLOC_RATE):
					#time.sleep((1000 - (current - self.executions[0])) / 1000)
					# User allocate too much instances, stop allocating a new one
					
					print(time.time(), 'Too many allocation', current, executions[0])
					return ''
					#raise Exception('Too many alloctions for user {}'.format(user))
				else:
					executions.append(current)
					break
			# Start new instance
			ekeys = list(self.scheduler.free_executors.keys())
			while True:
				e = ekeys[random.randint(0, len(ekeys)-1)]
				if (self.scheduler.free_executors[e].free > 0):
					break
			self.scheduler.free_executors[e].free -= 1
			if (self.scheduler.free_executors[e].free == 0):
				del self.scheduler.free_executors[e]
			iid = str(uuid.uuid4())
			stat = TaStatus(iid)
			# Start tasks
			stat.code = self.scheduler.executors[e].conn.run(iid, cmd)
			stat.start = time.time()
			self.scheduler.tasks[iid] = stat
			self.scheduler.free -= 1
			print("Execute task", iid)
			print(self.scheduler.cap, self.scheduler.free)
		except Exception as e:
			print("Error: " + str(e))
		finally:
			self.scheduler.smutex.release()
			schedalloc.release()
		return iid

	def info(self):
		#print(self.scheduler.cap, self.scheduler.free)
		data = ttypes.Info()
		data.cap = self.scheduler.cap
		data.free = self.scheduler.free
		data.min_cap = self.scheduler.minCap
		return data
	
	def add_user(self, name):
		self.executions[name] = []
		return 0
	
	def taskInfo(self, tid):
		if tid not in self.scheduler.tasks:
			return None
		else:
			task = self.scheduler.tasks[tid]
			info = ttypes.TaskStatus()
			info.code = task.code
			info.stdout = task.stdout
			info.stderr = task.stderr
			return info

class SchedulerIntHandler:
	def __init__(self, scheduler):
		self.log = {}
		self.scheduler = scheduler
	
	# Launch when an instance is reclaimed
	def finish(self, eid, iid, stat):
		schedalloc.acquire()
		self.scheduler.smutex.acquire()
		try:
			self.scheduler.executors[eid].free += 1
			self.scheduler.free += 1

			if (eid not in self.scheduler.free_executors):
				self.scheduler.free_executors[eid] = self.scheduler.executors[eid]
				schedalloc.notify()
			#del self.scheduler.tasks[iid]
			task = self.scheduler.tasks[iid]
			task.end = time.time()
			task.stdout = stat.stdout
			task.stderr = stat.stderr
			task.code = stat.code
			# Check if number of instances is too high, the we shrink to reduce 
			# wasting
			if (self.scheduler.free > common.BUFFER_SIZE
					and self.scheduler.cap > common.MIN_CAP):
				# Decide to reduce pool capacity, but first let reset the min cap to prevent
				# allocating new instances
				if self.scheduler.minCap >= self.scheduler.cap:
					self.scheduler.minCap -= common.BUFFER_SIZE
				# Find an executor and terminate 1 instance from it
				ekeys = list(self.scheduler.executors.keys())
				e = ekeys[random.randint(0, len(ekeys)-1)]
				self.scheduler.executors[e].conn.terminate(1)	
				
		except Exception as e:
			print(e)
		finally:
			print(self.scheduler.cap, self.scheduler.free)
			self.scheduler.smutex.release()
			schedalloc.release()
		print("Finish task", iid, "code: ", self.scheduler.tasks[iid].code)
		return 0
		
	def update(self, eid, stat):
		sched = self.scheduler
		schedalloc.acquire()
		sched.smutex.acquire()
		try:
			# Check executor connection existence
			if (eid not in sched.executors):
				sched.executors[eid] = ExStatus(eid, stat)
				# Add connection if it does not exist
				transport = TSocket.TSocket(stat.hostname, stat.port)
				transport = TTransport.TBufferedTransport(transport)
				protocol = TBinaryProtocol.TBinaryProtocol(transport)
				sched.executors[eid].conn = ExecutorInt.Client(protocol)
				transport.open()
				sched.executors[eid].trans = transport
				
			if (stat.cap < stat.free):
				raise Exception("Free exceeds Cap")
			ex = sched.executors[eid]
			cap = stat.cap - ex.cap
			#free = stat.free - ex.free
			sched.cap += cap
			#sched.free += free
			sched.free += cap
			
			# Update status
			sched.executors[eid].cap = stat.cap
			sched.executors[eid].free = stat.free
			sched.executors[eid].hostname = stat.hostname
			sched.executors[eid].port = stat.port
			if (stat.free > 0 and eid not in self.scheduler.free_executors):
				self.scheduler.free_executors[eid] = sched.executors[eid]
				schedalloc.notify()
		except Exception as e:
			print("Error: " + str(e))
		finally:
			sched.smutex.release()
			schedalloc.release()
		return 0
	
	def clean(self, eid, num):
		sched = self.scheduler
		sched.smutex.acquire()
		try:
			if (eid not in sched.executors):
				raise Exception('Got terminated message from unknown Executor {}'.format(eid))
			sched.executors[eid].cap -= num
			sched.cap -= num
			sched.free -= num
		except Exception as e:
			print("Error: " + str(e))
		finally:
			sched.smutex.release()
		print("Clean instances", sched.cap, sched.free)
		return 0
	


class RealtimeServerlessScheduler(Scheduler):

	def __init__(self, executor, port):
		self.executor = executor
		self.minCap = common.MIN_CAP
		self.cap = 0
		self.free = 0
		self.executors = {}
		self.free_executors = {}
		self.tasks = {}
		self.smutex = Lock()
		self.hostname = socket.gethostname()
		self.port = port
		self.server = None
		
		# Open connections for executors	
		print("Initializing...")
		handler = SchedulerIntHandler(self)
		processor = SchedulerInt.Processor(handler)
		transport = TSocket.TServerSocket(port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		server = TServer.TSimpleServer(processor, transport, tFactory, pFactory)
		
		def runserver(server):
			server.serve()

		thread = Thread(target=runserver, args=(server,))
		thread.setDaemon(True)
		thread.start()
		print("Initialized")
	
	def resourceOffers(self, driver, offers):
		filters = {'refuse_seconds': 1}

		cap = self.cap
		minCap = self.minCap
		print("Cap", cap, "Min Cap", minCap)
		for offer in offers:
			if (cap >= minCap):
				# Decline when lower bound is met
				driver.declineOffer(offer.id, filters=filters)
				continue

			cpus = self.getResource(offer.resources, 'cpus')
			mem = self.getResource(offer.resources, 'mem')
			#num_instances = int(min(cpus / common.TASK_CPU, mem / common.TASK_MEM,
			#		self.minCap - self.cap))
			num_instances = int(min((cpus - common.EXECUTOR_CPUS) / common.TASK_CPU, mem / common.TASK_MEM,
					self.minCap - self.cap))
			
			"""
			if cpus < TASK_CPU or mem < TASK_MEM:
				continue
			"""

			if (num_instances <= 0):
				# Decline because we can not use the resources
				continue

			# Otherwise, launch new instances
			cap += num_instances
			tasks = []
			for i in range(num_instances):
				task = Dict()
				task_id = str(uuid.uuid4())
				task.task_id.value = task_id
				task.agent_id.value = offer.agent_id.value
				task.name = 'task {}'.format(task_id)
				task.executor = self.executor
				#task.data = encode_data('Hello from task {}!'.format(task_id))
				task.data = encode_data('{}:{}:{}'.format(task_id, self.hostname, self.port))

				task.resources = [
					dict(name='cpus', type='SCALAR', scalar={'value': common.TASK_CPU}),
					dict(name='mem', type='SCALAR', scalar={'value': common.TASK_MEM}),
				]
				tasks.append(task)
			
			#driver.launchTasks(offer.id, [task], filters)
			driver.launchTasks(offer.id, tasks, filters)
			print("launch", num_instances, "task(s)")

	def getResource(self, res, name):
		for r in res:
			if r.name == name:
				return r.scalar.value
		return 0.0

	def statusUpdate(self, driver, update):
		logging.debug('Status update TID %s %s %s',
					  update.task_id.value,
					  update.state,
					  update.message)
		print(update.message)
		"""
		if update.state == 'TASK_FINISH' or update.state == 'TASK_KILLED':
			self.smutex.acquire()
			try:
				eid = update.task_id.value
				self.cap -= self.executor[eid].cap
				self.free -= self.executor[eid].free
				del self.executor[eid]
			finally:
				self.smutex.release()
		"""
		return 0
	
	def listen(self, port):
		import time
		
		while (self.cap < self.minCap):
			time.sleep(1)
		
		handler = RealtimeServerlessIntHandler(self)
		processor = RealtimeServerless.Processor(handler)
		transport = TSocket.TServerSocket(port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		#server = TServer.TSimpleServer(processor, transport, tFactory, pFactory)
		self.server = TServer.TThreadPoolServer(processor, transport, tFactory, pFactory)
		
		#print("Starting Real-time Serverless Pool...")
		print("Opened Real-time Serverless Pool")
		self.server.serve()
		#print("Real-time Serverless is ready")
		
	def stop(self):
		if (self.server != None):
			self.server.stop()
		
		
		

def main(master):
	executor = Dict()
	executor.executor_id.value = 'RealtimeServerlessExecutor'
	executor.name = executor.executor_id.value
	executor.command.value = '%s %s' % (
		sys.executable,
		abspath(join(dirname(__file__), 'realtime-serverless-executor.py'))
	)
	executor.resources = [
		dict(name='mem', type='SCALAR', scalar={'value': common.EXECUTOR_MEM}),
		dict(name='cpus', type='SCALAR', scalar={'value': common.EXECUTOR_CPUS}),
	]

	framework = Dict()
	framework.user = getpass.getuser()
	framework.name = "RealtimeServerlessFramework"
	framework.hostname = socket.gethostname()
	
	sched = RealtimeServerlessScheduler(executor, 12000)
	
	driver = MesosSchedulerDriver(
		sched,
		framework,
		master,
		use_addict=True,
	)
	
	def listen(sched, port):
		sched.listen(port)
	
	thread = Thread(target=listen, args=(sched, 12345))
	thread.setDaemon(True)
	thread.start()
	
	"""
	driver = MesosSchedulerDriver(
		RealtimeServerlessScheduler(executor),
		framework,
		master,
		use_addict=True,
	)
	"""

	def signal_handler(signal, frame):
		driver.stop()
	
	def run_driver_thread():
		driver.run()

	driver_thread = Thread(target=run_driver_thread, args=())
	driver_thread.start()

	print('Scheduler running, Ctrl+C to quit.')
	signal.signal(signal.SIGINT, signal_handler)

	while driver_thread.is_alive():
		time.sleep(1)
	

if __name__ == '__main__':
	import logging
	logging.basicConfig(level=logging.DEBUG)
	if len(sys.argv) != 2:
		print("Usage: {} <mesos_master>".format(sys.argv[0]))
		sys.exit(1)
	else:
		main(sys.argv[1])
