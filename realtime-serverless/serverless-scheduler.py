#!/usr/bin/env python2.7
from __future__ import print_function

import sys
import uuid
import time
import socket
import signal
import getpass
from threading import Thread, Lock, Condition
from os.path import abspath, join, dirname

from pymesos import MesosSchedulerDriver, Scheduler, encode_data
from addict import Dict

import datetime

import common

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless, SchedulerInt, ExecutorInt, ttypes

import random
import uuid
import time

class TaStatus:
	def __init__(self, iid):
		self.iid = iid
		self.cmd = ''
		self.stdout = ''
		self.stderr = ''
		self.start = 0
		self.end = 0
		self.code = ''
		self.wait = Condition()

class ExStatus:

	def __init__(self, eid):
		self.reset()
		self.eid = eid

	def __init__(self, eid, cap, free, hostname, port):
		self.reset()
		self.eid = eid
		self.cap = cap
		self.free = free
		self.hostname = hostname
		self.port = port
	
	def __init__(self, eid, stat):
		self.reset()
		self.eid = eid
		self.cap = stat.cap
		self.free = stat.free
		self.hostname = stat.hostname
		self.port = stat.port
	
	def reset(self):
		self.eid = ''
		self.cap = 0
		self.free = 0
		self.hostname = ""
		self.port = 0
		self.conn = None
		self.trans = None
	
	def close():
		if self.trans is not None:
			trans.close()




class RealtimeServerlessIntHandler:
	def __init__(self, scheduler):
		self.log = {}
		self.scheduler = scheduler
		self.hasinstance = Condition
		self.executions = {}
	
	def execute(self, user, cmd):
		if (user not in self.executions):
			return ''
		# Add to the queue and wait for execution
		iid = str(uuid.uuid4())
		stat = TaStatus(iid)
		stat.start = time.time()
		stat.cmd = cmd
		stat.iid = iid 
		# Start tasks
		stat.wait.acquire()
		#stat.code = self.scheduler.executors[user].conn.run(iid, cmd)
		self.scheduler.wtasks.append(stat)
		stat.wait.wait()
		stat.wait.release()
		self.scheduler.tasks[iid] = stat
		print("Execute task", iid)
		return iid

	def info(self):
		#print(self.scheduler.cap, self.scheduler.free)
		data = ttypes.Info()
		return data
	
	def add_user(self, name):
		self.executions[name] = []
		return 0

class SchedulerIntHandler:
	def __init__(self, scheduler):
		self.log = {}
		self.scheduler = scheduler
	
	# Launch when an instance is reclaimed
	def finish(self, eid, iid, stat):
		task = self.scheduler.tasks[iid]
		task.end = time.time()
		task.stdout = stat.stdout
		task.stderr = stat.stderr
		task.code = stat.code
		return 0
		
	def update(self, eid, stat):
		sched = self.scheduler
		sched.smutex.acquire()
		try:
			# Check executor connection existence
			if (eid not in sched.executors):
				sched.executors[eid] = ExStatus(eid, stat)
				# Add connection if it does not exist
				transport = TSocket.TSocket(stat.hostname, stat.port)
				transport = TTransport.TBufferedTransport(transport)
				protocol = TBinaryProtocol.TBinaryProtocol(transport)
				sched.executors[eid].conn = ExecutorInt.Client(protocol)
				transport.open()
				sched.executors[eid].trans = transport
				
			# Update status
			sched.executors[eid].cap = stat.cap
			sched.executors[eid].free = stat.free
			sched.executors[eid].hostname = stat.hostname
			sched.executors[eid].port = stat.port
		except Exception as e:
			print("Error: " + str(e))
		finally:
			sched.smutex.release()
		return 0
	
	def clean(self, eid, num):
		sched = self.scheduler
		sched.smutex.acquire()
		try:
			if (eid not in sched.executors):
				raise Exception('Got terminated message from unknown Executor {}'.format(eid))
			sched.executors[eid].cap -= num
			sched.cap -= num
			sched.free -= num
		except Exception as e:
			print("Error: " + str(e))
		finally:
			sched.smutex.release()
		print("Clean instances", sched.cap, sched.free)
		return 0
	


class ServerlessScheduler(Scheduler):

	def __init__(self, executor, port):
		self.executor = executor
		self.wtasks = []
		self.tasks = {}
		self.smutex = Lock()
		self.executors = {}
		
		# Open connections for executors	
		print("Initializing...")
		handler = SchedulerIntHandler(self)
		processor = SchedulerInt.Processor(handler)
		transport = TSocket.TServerSocket(port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		server = TServer.TSimpleServer(processor, transport, tFactory, pFactory)
		
		def runserver(server):
			server.serve()

		thread = Thread(target=runserver, args=(server,))
		thread.setDaemon(True)
		thread.start()
		print("Initialized")
	
	def resourceOffers(self, driver, offers):
		filters = {'refuse_seconds': 1}
		
		for offer in offers:
			self.smutex.acquire()
			try:
				if (len(self.wtasks) == 0):
					driver.declineOffer(offer.id)
					continue
				
				cpus = self.getResource(offer.resources, 'cpus')
				mem = self.getResource(offer.resources, 'mem')
				num_instances = int(min(cpus / common.TASK_CPU, mem / common.TASK_MEM))
				num_instances = min(num_instances, len(self.wtasks))
			
				if (num_instances <= 0):
					# Decline because we can not use the resources
					driver.declineOffer(offer.id)
					continue
					
				# Otherwise, launch new instances
				#cap += num_instances
				tasks = []
				for i in range(num_instances):
					data = self.wtasks.pop(0)
					data.wait.acquire()
					try:
						task = Dict()
						task.task_id.value = data.iid
						task.agent_id.value = offer.agent_id.value
						task.name = 'task {}'.format(data.iid)
						task.executor = self.executor
						task.data = encode_data(data.cmd)
						data.wait.notify()
					finally:
						data.wait.release()
					
					task.resources = [
						dict(name='cpus', type='SCALAR', scalar={'value': common.TASK_CPU}),
						dict(name='mem', type='SCALAR', scalar={'value': common.TASK_MEM}),
					]
					tasks.append(task)
			
				driver.launchTasks(offer.id, tasks, filters)
				#driver.launchTasks(offer.id, tasks)
				print("launch", num_instances, "task(s)")
			finally:
				self.smutex.release()
			
		
	def getResource(self, res, name):
		for r in res:
			if r.name == name:
				return r.scalar.value
		return 0.0

	def statusUpdate(self, driver, update):
		logging.debug('Status update TID %s %s %s',
					  update.task_id.value,
					  update.state,
					  update.message)
		print(update.message)
		return 0
	
	def listen(self, hostname, port):
		import time
		
		handler = RealtimeServerlessIntHandler(self)
		processor = RealtimeServerless.Processor(handler)
		transport = TSocket.TServerSocket(host=hostname, port=port)
		tFactory = TTransport.TBufferedTransportFactory()
		pFactory = TBinaryProtocol.TBinaryProtocolFactory()

		#server = TServer.TSimpleServer(processor, transport, tFactory, pFactory)
		self.server = TServer.TThreadPoolServer(processor, transport, tFactory, pFactory)
		
		#print("Starting Real-time Serverless Pool...")
		print("Opened Serverless Pool")
		self.server.serve()
		#print("Real-time Serverless is ready")
		
	def stop(self):
		if (self.server != None):
			self.server.stop()
		
		
		

def main(master, hostname):
	executor = Dict()
	executor.executor_id.value = 'RealtimeServerlessExecutor'
	executor.name = executor.executor_id.value
	executor.command.value = '%s %s' % (
		sys.executable,
		abspath(join(dirname(__file__), 'serverless-executor.py'))
	)
	executor.resources = [
		dict(name='mem', type='SCALAR', scalar={'value': common.EXECUTOR_MEM}),
		dict(name='cpus', type='SCALAR', scalar={'value': common.EXECUTOR_CPUS}),
	]

	framework = Dict()
	framework.user = getpass.getuser()
	framework.name = "ServerlessFramework"
	framework.hostname = socket.gethostname()
	
	#sched = RealtimeServerlessScheduler(executor, 12001)
	sched = ServerlessScheduler(executor, 12001)
	
	driver = MesosSchedulerDriver(
		sched,
		framework,
		master,
		use_addict=True,
	)
	
	def listen(sched, hostname, port):
		sched.listen(hostname, port)
	
	#thread = Thread(target=listen, args=(sched, 12345))
	thread = Thread(target=listen, args=(sched, hostname, 12346))
	thread.setDaemon(True)
	thread.start()
	
	"""
	driver = MesosSchedulerDriver(
		RealtimeServerlessScheduler(executor),
		framework,
		master,
		use_addict=True,
	)
	"""

	def signal_handler(signal, frame):
		driver.stop()

	def run_driver_thread():
		driver.run()

	driver_thread = Thread(target=run_driver_thread, args=())
	driver_thread.start()

	print('Scheduler running, Ctrl+C to quit.')
	signal.signal(signal.SIGINT, signal_handler)

	while driver_thread.is_alive():
		time.sleep(1)


if __name__ == '__main__':
	import logging
	logging.basicConfig(level=logging.DEBUG)
	if len(sys.argv) != 3:
		print("Usage: {} <mesos_master> <scheduler>".format(sys.argv[0]))
		sys.exit(1)
	else:
		main(sys.argv[1], sys.argv[2])
