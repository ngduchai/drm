#!/usr/bin/env python2.7
from __future__ import print_function

import sys
import uuid
import time
import socket
import signal
import getpass
from threading import Thread, Lock
from os.path import abspath, join, dirname

from pymesos import MesosSchedulerDriver, Scheduler, encode_data
from addict import Dict

#TASK_CPU = 0.1
#TASK_MEM = 32
#EXECUTOR_CPUS = 0.1
#EXECUTOR_MEM = 32

import common

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from pyintf.rtsl import RealtimeServerless

from time import sleep

if (len(sys.argv) != 3):
	print("Usage: {} <realtime-serverless> <port>".format(sys.argv[0]))
	sys.exit(1)
	
hostname = sys.argv[1]
port = sys.argv[2]

allocated_instances = []
alock = Lock()

def collect_info(duration):
	
	global allocated_instances

	transport = TSocket.TSocket(hostname, port)
	transport = TTransport.TBufferedTransport(transport)
	protocol = TBinaryProtocol.TBinaryProtocol(transport)
	client = RealtimeServerless.Client(protocol)
		
	transport.open()
	
	start = time.time()
	while (True):
		data = client.info()
		#print(time.time(), data.cap, data.free, data.min_cap)
		alock.acquire()
		current = time.time()
		while (len(allocated_instances) > 0 and current - allocated_instances[0] > duration):
			allocated_instances.pop(0)
		alloc = len(allocated_instances)
		alock.release()
		print(current - start, alloc)
		sleep(1)
	
	transport.close()

def run_function(name, num, rate):
	
	global allocated_instances
	
	transport = TSocket.TSocket(hostname, port)
	transport = TTransport.TBufferedTransport(transport)
	protocol = TBinaryProtocol.TBinaryProtocol(transport)
	client = RealtimeServerless.Client(protocol)
		
	transport.open()
	
	client.add_user(name)
	
	for i in range(num):
		iid = ''
		while (iid == ''):
			#iid = client.execute(name, "sleep 17")
			iid = client.execute(name, "sleep 15")
			sleep(1/float(rate))
		alock.acquire()
		allocated_instances.append(time.time())
		alock.release()
	
	print("Finish function call")

	transport.close()



thread = Thread(target=collect_info, args=(5,))
thread.setDaemon(True)
thread.start()

num = 1000
#users = ['test1', 'test2']
users = ['test']

for user in users:
	thread = Thread(target=run_function, args=(user, num, 10))
	thread.setDaemon(True)
	thread.start()

while (True):
	time.sleep(100)
	



